using System;
using System.Collections;
using System.Collections.Generic;
using _3C;
using TMPro;
using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField] private PlayerCharacter _player;
    [SerializeField] private TMP_Text _remainingPitons;

    private void Update()
    {
        //TODO: intégrer plus proprement avec des delegates
        if (_player)
        {
            if (_remainingPitons)
                _remainingPitons.text = _player.currentPitonCount.ToString();
        }
    }
}
