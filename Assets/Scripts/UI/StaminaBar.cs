using System;
using System.Collections;
using System.Collections.Generic;
using _3C;
using UnityEngine;

public class StaminaBar : MonoBehaviour
{
    [SerializeField] private Sprite[] sprites;

    private SpriteRenderer _renderer;
    private PlayerCharacter _player;
    
    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
        _player = GetComponentInParent<PlayerCharacter>();
        _player.onStaminaChanged.AddListener(OnStaminaChanged);
    }

    private void OnStaminaChanged(float newStamina)
    {
        int staminaValue = Mathf.RoundToInt(Mathf.Clamp((newStamina * (float)(sprites.Length-1))/_player.maxStamina, 0.0f, (float)(sprites.Length-1)));
        _renderer.sprite = sprites[staminaValue];

        _renderer.enabled = newStamina < _player.maxStamina;
    }
}
