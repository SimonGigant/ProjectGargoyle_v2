using PGS.Input;

public static class InputUtils
{
    private const string Vector2D = "Vector2";

    public static bool IsPressed(MyInputMap map, InputButton button)
    {
        if (map.Disabled)
        {
            return false;
        }

        return map.States[button.Id].IsPressed;
    }

    public static UnityEngine.Vector2 GetValue(MyInputMap map, Analog2D analog)
    {
        if (map.Disabled)
        {
            return UnityEngine.Vector2.zero;
        }

        return map.States[analog.Id].Direction;
    }

    public static float GetValue(MyInputMap map, Analog1D button)
    {
        if (map.Disabled)
        {
            return 0;
        }

        return map.States[button.Id].AnalogValue;
    }

    public static bool GetValue(MyInputMap map, InputButton button)
    {
        if (map.Disabled)
        {
            return false;
        }

        return map.States[button.Id].IsPressed;
    }

    public static float TimeSinceLastInput(MyInputMap map, MyInputControl button)
    {
        return map.States[button.Id].TimeSinceLastInput;
    }

    public static bool IsActionTriggered(MyInputMap map, MyInputControl button)
    {
        if (map.Disabled)
        {
            return false;
        }

        return map.States[button.Id].IsActionTriggered;
    }

    public static bool OnRelease(MyInputMap map, MyInputControl button)
    {
        if (map.Disabled)
        {
            return false;
        }

        return map.States[button.Id].Release;
    }

    public static bool OnPress(MyInputMap map, MyInputControl button)
    {
        if (map.Disabled)
        {
            return false;
        }

        return map.States[button.Id].Started;
    }

    public static float GetFloatValueFromAction(UnityEngine.InputSystem.InputAction action)
    {
        switch (action.expectedControlType)
        {
            case Vector2D:
                UnityEngine.Debug.LogError($"Wrong ActionType Requested, expected Type = Float");
                break;
            default:
                return action.ReadValue<float>();
        }

        return .0f;
    }

    public static UnityEngine.Vector2 GetVector2ValueFromAction(UnityEngine.InputSystem.InputAction action)
    {
        switch (action.expectedControlType)
        {
            case Vector2D:
                return action.ReadValue<UnityEngine.Vector2>();
            default:
                UnityEngine.Debug.LogError($"Wrong ActionType Requested, expected Type = Vector2");
                break;
        }

        return UnityEngine.Vector2.zero;
    }
}