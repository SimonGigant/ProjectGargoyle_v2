namespace PGS.Input
{
    public partial struct InputState
    {
        public UnityEngine.Vector2 Direction;
        public bool IsPressed;
        public float AnalogValue;
        public UnityEngine.InputSystem.InputAction Action;
        public float TimeSinceLastInput;
        public bool Started;
        public bool Canceled;
        public bool Performed;
        public bool Release;
        public bool IsActionTriggered;

        public InputState(UnityEngine.InputSystem.InputAction action)
        {
            this.Direction = UnityEngine.Vector2.zero;
            this.IsPressed = false;
            this.AnalogValue = .0f;
            this.Action = action;
            this.TimeSinceLastInput = .0f;
            this.Started = false;
            this.Canceled = false;
            this.Performed = false;
            this.Release = false;
            this.IsActionTriggered = false;
        }
    }
}