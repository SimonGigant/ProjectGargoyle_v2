require "erb"
require "json"

def gen(filename, _context)
    content = File.read(filename);
    erb = ERB.new(content, trim_mode: '-');
    c = erb.result(_context);

    return c;
end

def genTemplate(filename)
    jsonContent = File.read(filename)
    json = JSON.parse(jsonContent);

    my_context = binding;
    output = gen("template.cs.erb", my_context)

    File.write("GeneratedFiles/My#{json["name"]}.cs", output)

    return json
end

test = Dir.entries("..\\..\\..\\Assets\\Input\\")

list = Array.new

test.each do |action|
    if action.include? ".inputactions"
        if !action.include? ".meta"
            list.push(genTemplate("..\\..\\..\\Assets\\Input\\"+action))
            puts 'Generate Input for ' + action
        end
    end
end

# list.each do |json|
#     json["maps"].each do |map|
#         puts json["name"] + "_" + map["name"];
#     end
# end

genTemplateInputSystem(list);