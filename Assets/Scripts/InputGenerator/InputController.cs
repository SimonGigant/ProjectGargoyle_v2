using PGS.Input;

public class InputController : PGS.Game.Component
{
    public UnityEngine.InputSystem.InputActionAsset playerActionMap;


    public override void OnStart()
    {
        InputMapUtils.PlayerMap = MyPlayerInput.Build(playerActionMap.FindActionMap(MyPlayerInput.MapName, true));
    }

    public override void UpdateComponent(float dt)
    {
        InputMapUtils.PlayerMap.Update(dt);
    }
}