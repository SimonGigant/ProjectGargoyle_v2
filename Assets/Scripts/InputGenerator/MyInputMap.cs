namespace PGS.Input
{
    public class MyInputMap
    {
        public InputState[] States;
        public bool Disabled;

        private const string Axis = "Axis";
        private const string Vector2D = "Vector2";
        private const string Button = "Button";

        public void Init(bool enabled = true)
        {
            this.Disabled = !enabled;
        }

        public void Update(float dt)
        {
            if (this.Disabled)
            {
                return;
            }

            for (int i = 0; i < this.States.Length; ++i)
            {
                var newVectorValue = UnityEngine.Vector2.zero;
                var newAnalogValue = .0f;
                var newPressedValue = false;

                this.States[i].TimeSinceLastInput += dt;
                var isTriggered = this.States[i].Action.triggered;

                switch (this.States[i].Action.expectedControlType)
                {
                    case Vector2D:
                        newVectorValue = InputUtils.GetVector2ValueFromAction(this.States[i].Action);
                        break;
                    case Axis:
                    case Button:
                        newAnalogValue = InputUtils.GetFloatValueFromAction(this.States[i].Action);
                        newPressedValue = InputUtils.GetFloatValueFromAction(this.States[i].Action) > 0.5f;
                        break;
                }

                var newDirectionDifferent = this.States[i].Direction != newVectorValue;
                var newAnalogDifferent = this.States[i].AnalogValue != newAnalogValue;
                var newPressedDifferent = this.States[i].IsPressed != newPressedValue;

                if (newDirectionDifferent || newAnalogDifferent || newPressedDifferent)
                {
                    this.States[i].TimeSinceLastInput = .0f;
                }

                this.States[i].Direction = newVectorValue;
                this.States[i].AnalogValue = newAnalogValue;
                this.States[i].IsActionTriggered = isTriggered;

                if (newPressedValue != this.States[i].IsPressed)
                {
                    this.States[i].Release = !newPressedValue;
                    this.States[i].Started = newPressedValue;

                }
                else
                {
                    this.States[i].Started = false;
                    this.States[i].Release = false;
                }

                this.States[i].IsPressed = newPressedValue;
            }
        }

        public void Disable()
        {
            this.Disabled = true;
        }

        public void Enable()
        {
            this.Disabled = false;
        }

        public bool IsEnabled()
        {
            return this.Disabled == false;
        }
    }
}

public abstract class MyInputControl
{
    public int Id;
    public string Name;
}

public class InputButton : MyInputControl
{
}

public class Analog2D : MyInputControl
{
}

public class Analog1D : MyInputControl
{
}
