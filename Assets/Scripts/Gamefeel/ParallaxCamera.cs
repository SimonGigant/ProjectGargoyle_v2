﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxCamera : MonoBehaviour
{
    [SerializeField] private Transform follow = null;
    private Vector2 startPos;
    private Vector2 target;

    private void Start()
    {
        startPos = transform.position;
    }

    private void Update()
    {
        target = Vector2.Lerp(startPos, follow.position, 0.12f);
        Vector2 currentPos = Vector2.Lerp(transform.position, target, Time.deltaTime * 4f);
        transform.position = new Vector3(currentPos.x, currentPos.y, transform.position.z);
    }
}
