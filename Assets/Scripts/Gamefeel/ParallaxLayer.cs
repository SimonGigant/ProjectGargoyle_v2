﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxLayer : MonoBehaviour
{
    [SerializeField] private float layer = 0f;
    [SerializeField] private bool horizontalParallax = true;
    [SerializeField] private bool verticalParallax = true;
    private Vector2 _startpos;
    private Camera _cam;

    private void Start()
    {
        _startpos = transform.position;
        _cam = Camera.main;
    }

    private void Update()
    {
        Vector2 dist = (_cam.transform.position * layer);
        if (!horizontalParallax)
            dist.x = 0f;
        if (!verticalParallax)
            dist.y = 0f;
        transform.position = new Vector3(_startpos.x + dist.x, _startpos.y + dist.y, transform.position.z);
    }
}
