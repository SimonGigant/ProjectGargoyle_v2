﻿using System.Collections;
using System.Collections.Generic;
using PowerTools;
using UnityEngine;

public class VFXSpawned : MonoBehaviour
{

    private SpriteAnim _anim;
    public AnimationClip animationToPlay;
    void Start()
    {
            _anim = GetComponent<SpriteAnim>();
            _anim.Play(animationToPlay);
            StartCoroutine(WaitBeforeDestroy());
    }
    private IEnumerator WaitBeforeDestroy()
    {
        yield return new WaitForSeconds(_anim.GetCurrentAnimation().length);
        Destroy(gameObject);
    }
}
