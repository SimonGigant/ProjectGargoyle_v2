using System.Collections;
using System.Collections.Generic;
using _3C;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	
    private static GameManager _instance;

    public static GameManager Instance
    {
	    get {
		    if (_instance == null)
		    {
			    _instance = GameObject.FindObjectOfType<GameManager>();
			    if (_instance == null)
			    {
				    GameObject container = new GameObject("GameManager");
				    _instance = container.AddComponent<GameManager>();
			    }
		    }
		    return _instance;
	    }
    }

    public PlayerCharacter Player;

    public void RespawnScene()
    {
	    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
