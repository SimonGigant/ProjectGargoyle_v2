﻿using System.Collections;
using System.Collections.Generic;
using _3C;
using GigSpeech;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Trigger : MonoBehaviour
{
    public ProgressionConditionHandler unlockCondition;
    
    
    [SerializeField] private bool singleTimeTrigger;
    private bool _wasTriggeredIn;
    private bool _wasTriggeredOut;


    [SerializeField] private UnityEvent triggerIn;
    [SerializeField] private UnityEvent triggerOut;

    [HideInInspector] public bool characterIsInTrigger;

    private void TryTrigger()
    {
        if (unlockCondition.ComputeCondition())
        {
            if (!(singleTimeTrigger && _wasTriggeredIn))
            {
                triggerIn.Invoke();
                _wasTriggeredIn = true;
            }
            characterIsInTrigger = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent<PlayerCharacter>(out PlayerCharacter character))
        {
            TryTrigger();
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.TryGetComponent<PlayerCharacter>(out PlayerCharacter character))
        {
            TryTrigger();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent<PlayerCharacter>(out PlayerCharacter character)){
            if (!(singleTimeTrigger && _wasTriggeredOut))
            {
                triggerOut.Invoke();
                _wasTriggeredOut = true;
            }
            characterIsInTrigger = false;
        }
    }
}
