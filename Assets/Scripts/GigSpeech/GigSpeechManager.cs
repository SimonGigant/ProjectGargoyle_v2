using System;
using UnityEngine;

namespace GigSpeech
{
    public class GigSpeechManager : MonoBehaviour
    {
        private static GigSpeechManager _instance;

        public static bool DoesInstanceExist()
        {
            return _instance != null;
        }

        public static GigSpeechManager Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<GigSpeechManager>();
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("GigSpeechManager");
                        DontDestroyOnLoad(container);
                        _instance = container.AddComponent<GigSpeechManager>();
                    }
                }
                return _instance;
            }
        }
        
        
        public GigSpeechParameters parameters;
        
        //Delegate Definitions
        public delegate void DialogueEventDelegate();
        public delegate void DialogueLineEventDelegate(GigSpeechSpeakerData speaker);
        
        //Callbacks Delegates
        public DialogueEventDelegate OnDialogueBegin;
        public DialogueEventDelegate OnDialogueEnd;
        public DialogueEventDelegate OnSkipDialogue;
        public DialogueLineEventDelegate OnDialogueLineBegin;
    }
}
