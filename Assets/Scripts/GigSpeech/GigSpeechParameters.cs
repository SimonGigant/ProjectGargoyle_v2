﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace GigSpeech
{

    [System.Serializable]
    public class GigSpeechBoxData
    {
        public string name;
        
        [PreviewField(150)]
        [AssetSelector(Paths = "Assets/Art/UI/DialogueBox", FlattenTreeView = true)]
        public Sprite neutralBox;
        
        [PreviewField(150)]
        [AssetSelector(Paths = "Assets/Art/UI/DialogueBox", FlattenTreeView = true)]
        public Sprite leftBox;
        
        [PreviewField(150)]
        [AssetSelector(Paths = "Assets/Art/UI/DialogueBox", FlattenTreeView = true)]
        public Sprite rightBox;

        public bool shakeWhenSpeak;
    }
    
    
    public enum GigSpeechPortraitAnimType {None, PuppetRotation, PuppetTranslation, Scale}
    public enum GigSpeechPortraitTransitionType { None, Basic}
    public enum GigSpeechBoxEffect { Base, Yell}

    [CreateAssetMenu(fileName = "New Parameter", menuName = "GigSpeech/GigSpeechParameters")]
    [Serializable]
    public class GigSpeechParameters : ScriptableObject
    {
        [ListDrawerSettings(NumberOfItemsPerPage = 1)]
        [TabGroup("Box")]
        public List<GigSpeechBoxData> boxData;
        
        [TabGroup("Box")]
        [SerializeField] public bool twinklingDotWhenCanPass;
        
        [TabGroup("Portraits")]
        [SerializeField] public bool showListener = true;
        [TabGroup("Portraits")]
        [ShowIf("showListener")]
        [SerializeField] public Color listenerMultiplyColor = Color.grey;
        [TabGroup("Portraits")]
        [SerializeField] public GigSpeechPortraitAnimType animType;
        [TabGroup("Portraits")]
        [SerializeField] public GigSpeechPortraitTransitionType transitionType;
        [TabGroup("Portraits")]
        public float portraitShakeIntensity = 1.0f;
        [TabGroup("Portraits")]
        public float durationAnimationEnteringBox = 0.6f;
        
        [TabGroup("Text")]
        [ColorPalette]
        public Color baseColor;
        [TabGroup("Text")]
        [ColorPalette]
        public Color colorAparte;
        [TabGroup("Text")]
        public float delayBetweenLetters = 1.0f;
        [TabGroup("Text")]
        public bool noSpeakerIsItalic = true;
        [TabGroup("Text")]
        public bool autoPass = true;
        [ShowIf("autoPass")]
        [TabGroup("Text")]
        public float autoPassDuration = 3.0f;
        [TabGroup("Text")]
        public float letterAnimationIntensity = 1.0f;

        
        
        [TabGroup("Prefabs")]
        [SerializeField]
        [PreviewField(300)]
        public GameObject dialoguePrefab;
        
        [TabGroup("Paths")]
        [FolderPath(RequireExistingPath = true)]
        public string dialoguesPath = "Assets/Dialogues";
        [TabGroup("Paths")]
        [FolderPath(RequireExistingPath = true)]
        public string speakersPath = "Assets/Dialogues/Speakers";
    }
}