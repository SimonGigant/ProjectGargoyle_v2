using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using GigSpeech;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;


[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
[Conditional("UNITY_EDITOR")]
public class PortraitAttribute : Attribute
{
    public PortraitAttribute()
    {
    }
}

public class PortraitDrawer : OdinAttributeDrawer<PortraitAttribute>
{
    private InspectorProperty _speakerGroupProperty;
    private InspectorProperty _listenerGroupProperty;
    
    private InspectorProperty _speakerDataProperty;
    private InspectorProperty _speakerMirrorProperty;
    private InspectorProperty _speakerEmotionIDProperty;
    private InspectorProperty _speakerLeftSideProperty;

    private InspectorProperty _listenerOverrideProperty;
    private InspectorProperty _listenerDataProperty;
    private InspectorProperty _listenerMirrorProperty;
    private InspectorProperty _listenerEmotionIDProperty;

    private readonly string _speakerDataPropertyName = "speaker";
    private readonly string _speakerEmotionPropertyName = "emotionID";
    private readonly string _speakerMirrorPropertyName = "mirror";
    private readonly string _speakerLeftSidePropertyName = "leftSide";
    
    private readonly string _listenerOverridePropertyName = "overrideListener";
    private readonly string _listenerDataPropertyName = "listenerOverridden";
    private readonly string _listenerMirrorPropertyName = "listenerMirror";
    private readonly string _listenerEmotionIDPropertyName = "listenerEmotionID";

    private InspectorProperty _parentProperty;

    private InspectorProperty GetPropertyFromName(string propertyName)
    {
        bool Predicate(InspectorProperty property)
        {
            return propertyName == property.Name;
        }

        return _parentProperty.FindChild(Predicate, true);
    }

    private InspectorProperty GetParentProperty(string childName)
    {
        bool Predicate(InspectorProperty property)
        {
            return property.Children.Get(childName) != null;
        }

        return _parentProperty.FindChild(Predicate, true);
    }

    protected override void Initialize()
    {
        _parentProperty = this.Property.Parent.Parent;
        _speakerGroupProperty = _parentProperty.Children.Get("#tabSpeakers").Children.Get("#Speaker");
        _listenerGroupProperty = _parentProperty.Children.Get("#tabSpeakers").Children.Get("#Listener");

        this._speakerDataProperty = _speakerGroupProperty.Children.Get(_speakerDataPropertyName);
        this._speakerMirrorProperty = _speakerGroupProperty.Children.Get(_speakerMirrorPropertyName);
        this._speakerEmotionIDProperty = _speakerGroupProperty.Children.Get(_speakerEmotionPropertyName);
        this._speakerLeftSideProperty = _speakerGroupProperty.Children.Get(_speakerLeftSidePropertyName);
        
        this._listenerDataProperty = _listenerGroupProperty.Children.Get(_listenerDataPropertyName);
        this._listenerMirrorProperty = _listenerGroupProperty.Children.Get(_listenerMirrorPropertyName);
        this._listenerEmotionIDProperty = _listenerGroupProperty.Children.Get(_listenerEmotionIDPropertyName);
        this._listenerOverrideProperty = _listenerGroupProperty.Children.Get(_listenerOverridePropertyName);
    }

    private void PrintErrorNotMember(string attributeString, InspectorProperty parent)
    {
        if (parent == null)
        {
            SirenixEditorGUI.ErrorMessageBox("Parent is null");
            return;
        }
        SirenixEditorGUI.ErrorMessageBox(attributeString + " is not a member of " + parent.NiceName + ".");
        SirenixEditorGUI.ErrorMessageBox("List of children:");
        foreach (InspectorProperty children in parent.Children)
        {
            SirenixEditorGUI.ErrorMessageBox(children.Name);
        }
    }
    
    protected override void DrawPropertyLayout(GUIContent label)
    {
        Rect rect = EditorGUILayout.GetControlRect();

        if(label != null)
            rect = EditorGUI.PrefixLabel(rect, label);

        if (_speakerDataProperty != null && _speakerMirrorProperty != null && _speakerEmotionIDProperty != null && _speakerLeftSideProperty != null)
        {
            Rect valuesRect = EditorGUILayout.GetControlRect(false, 300);
            float minDimension = valuesRect.height > valuesRect.width/2 ? valuesRect.width/2 : valuesRect.height;

            Rect leftRect = valuesRect;
            Rect rightRect = valuesRect;
            rightRect.x = leftRect.x + leftRect.width - minDimension;

            bool leftSide = (bool)_speakerLeftSideProperty.ValueEntry.WeakSmartValue;

            GigSpeechSpeakerData speakerData = (GigSpeechSpeakerData)_speakerDataProperty.ValueEntry.WeakSmartValue;
            bool speakerMirror = (bool)_speakerMirrorProperty.ValueEntry.WeakSmartValue;
            int speakerEmotionID = (int)_speakerEmotionIDProperty.ValueEntry.WeakSmartValue;
            if (speakerEmotionID < speakerData.portraits.Count)
            {
                Sprite speakerSprite = speakerData.portraits[speakerEmotionID].sprite;
                PrintTexture(leftSide ? leftRect : rightRect, minDimension, speakerSprite, speakerMirror);
            }

            if (_listenerOverrideProperty != null && _listenerDataProperty != null && _listenerMirrorProperty != null &&
                _listenerEmotionIDProperty != null)
            {
                bool overrideListener = (bool)_listenerOverrideProperty.ValueEntry.WeakSmartValue;
                if (overrideListener)
                {
                    GigSpeechSpeakerData listenerData =
                        (GigSpeechSpeakerData)_listenerDataProperty.ValueEntry.WeakSmartValue;
                    if (listenerData != null)
                    {
                        bool listenerMirror = (bool)_listenerMirrorProperty.ValueEntry.WeakSmartValue;
                        int listenerEmotionID = (int)_listenerEmotionIDProperty.ValueEntry.WeakSmartValue;
                        if (listenerEmotionID < listenerData.portraits.Count)
                        {
                            Sprite listenerSprite = listenerData.portraits[listenerEmotionID].sprite;
                            PrintTexture(leftSide ? rightRect : leftRect, minDimension, listenerSprite, listenerMirror);
                        }
                    }
                }
            }
        }
    }

    private void PrintTexture(Rect portraitRect, float minDimension, Sprite portraitSprite, bool mirror)
    {
        Rect drawRect = new Rect(portraitRect.position, new Vector2(minDimension, minDimension));
        if (mirror)
        {
            drawRect.x += minDimension;
            drawRect.width = -drawRect.width;
        }
        GUI.DrawTexture(drawRect, portraitSprite.texture);
    }
}
