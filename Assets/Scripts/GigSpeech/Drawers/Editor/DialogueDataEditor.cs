using System.Collections;
using System.Collections.Generic;
using System.IO;
using GigSpeech;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using Unity.VisualScripting;

public enum DialogueDataEditorTabs { Speakers, DialogueSequences}

public class DialogueDataEditor : OdinMenuEditorWindow
{
    private CreateNewDialogue _createNewDialogue;
    private CreateNewSpeaker _createNewSpeaker;
    
    private DialogueDataEditorTabs _selectedTab;

    protected override void OnDestroy()
    {
        base.OnDestroy();
    }
    
    [MenuItem("Tools/GigSpeech/DialogueEditor")]
    private static void OpenWindow()
    {
        GetWindow<DialogueDataEditor>().Show();
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        OdinMenuTree tree = new OdinMenuTree();

        switch (_selectedTab)
        {
            case DialogueDataEditorTabs.Speakers:
            {
                tree.AddAllAssetsAtPath("Speaker", "Assets/Dialogues/Speakers", typeof(GigSpeechSpeakerData), true, false);
                _createNewSpeaker = new CreateNewSpeaker();
                tree.Add("Create New", _createNewSpeaker);
                break;
            }
            case DialogueDataEditorTabs.DialogueSequences:
            {
                tree.AddAllAssetsAtPath("Dialogue Data", "Assets/Dialogues", typeof(GigSpeechDialogueSequence), true, false);
                _createNewDialogue = new CreateNewDialogue();
                tree.Add("Create New",_createNewDialogue);
                break;
            }
        }
        return tree;
    }

    private static bool SelectButton(Rect rect, string name, bool selected)
    {
        if (GUI.Button(rect, GUIContent.none, GUIStyle.none))
            return true;

        if (Event.current.type == EventType.Repaint)
        {
            var style = new GUIStyle(EditorStyles.miniButtonMid);
            style.stretchHeight = true;
            style.fixedHeight = rect.height;
            style.Draw(rect, GUIHelper.TempContent(name), false, false, selected, false);
        }

        return false;
    }

    protected override void OnBeginDrawEditors()
    {
        OdinMenuTreeSelection selected = this.MenuTree.Selection;


        Rect rect = GUILayoutUtility.GetRect(0, 25);
        DialogueDataEditorTabs[] tabsToDraw = {DialogueDataEditorTabs.DialogueSequences, DialogueDataEditorTabs.Speakers};
        for (int i = 0; i < tabsToDraw.Length; ++i)
        {
            Rect btnRect = rect.Split(i, tabsToDraw.Length);

            if (SelectButton(btnRect, tabsToDraw[i].ToString(), tabsToDraw[i] == _selectedTab))
            {
                _selectedTab = tabsToDraw[i];
                this.ForceMenuTreeRebuild();
                break;
            }
        }
        
        SirenixEditorGUI.BeginHorizontalToolbar();
        {
            GUILayout.FlexibleSpace();

            if (SirenixEditorGUI.ToolbarButton("Delete"))
            {
                GigSpeechDialogueSequence asset = selected.SelectedValue as GigSpeechDialogueSequence;
                string path = AssetDatabase.GetAssetPath(asset);
                AssetDatabase.DeleteAsset(path);
                AssetDatabase.SaveAssets();
            }
        }
        SirenixEditorGUI.EndHorizontalToolbar();
    }


    public class CreateNewDialogue
    {
        [FolderPath(RequireExistingPath = true)]
        [LabelWidth(100)]
        public string folderPath;
        [LabelWidth(100)]
        public string assetName;
        
        public CreateNewDialogue()
        {
            folderPath = "Assets/Dialogues";
            assetName = "New Dialogue";
        }


        [Button("Add New DialogueSequence")]
        private void CreateNewData()
        {
            GigSpeechDialogueSequence dialogueSequence = ScriptableObject.CreateInstance<GigSpeechDialogueSequence>();
            dialogueSequence.dialogues = new List<GigSpeechDialogueBitData>(1);
            dialogueSequence.dialogues.Add(new GigSpeechDialogueBitData());
            
            AssetDatabase.CreateAsset(dialogueSequence, folderPath + "/" + assetName + ".asset");
            AssetDatabase.SaveAssets();
        }
    }

    public class CreateNewSpeaker
    {
        [FolderPath(RequireExistingPath = true)]
        [LabelWidth(100)]
        public string folderPath;
        [LabelWidth(100)]
        public string assetName;
        
        public CreateNewSpeaker()
        {
            folderPath = "Assets/Dialogues/Speakers";
            assetName = "New Speaker";
        }
        
        [Button("Add New Speaker")]
        private void CreateNewData()
        {
            GigSpeechSpeakerData speaker = ScriptableObject.CreateInstance<GigSpeechSpeakerData>();
            speaker.name = assetName;
            
            AssetDatabase.CreateAsset(speaker, folderPath + "/" + assetName + ".asset");
            AssetDatabase.SaveAssets();
        }
    }
}
