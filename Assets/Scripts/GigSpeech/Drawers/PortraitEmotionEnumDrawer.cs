using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector.Editor;
using UnityEngine;
using System;
using System.Diagnostics;
using GigSpeech;
using Sirenix.Utilities.Editor;
using UnityEditor;

[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
[Conditional("UNITY_EDITOR")]
public class PortraitEmotionEnumAttribute : Attribute
{
    public string speaker;

    public PortraitEmotionEnumAttribute(string speaker)
    {
        this.speaker = speaker;
    }
}

public class PortraitEmotionEnumDrawer : OdinAttributeDrawer<PortraitEmotionEnumAttribute, int>
{
    
    private InspectorProperty _speakerDataProperty;

    protected override void Initialize()
    {
        this._speakerDataProperty = this.Property.Parent.Children.Get(this.Attribute.speaker);
    }

    protected override void DrawPropertyLayout(GUIContent label)
    {
        Rect rect = EditorGUILayout.GetControlRect();

        if(label != null)
            rect = EditorGUI.PrefixLabel(rect, label);
        
        if (this._speakerDataProperty == null)
            SirenixEditorGUI.ErrorMessageBox(this.Attribute.speaker + " is not a member of " + this.Property.Parent.NiceName + ".");
        else if (this._speakerDataProperty.ValueEntry.TypeOfValue != typeof (GigSpeechSpeakerData))
        {
            SirenixEditorGUI.ErrorMessageBox(this.Attribute.speaker + " on " + this.Property.Parent.NiceName + "  must be a GigSpeechSpeakerData.");
        }

        int value = this.ValueEntry.SmartValue;

        if (_speakerDataProperty != null)
        {
            GigSpeechSpeakerData speakerData = (GigSpeechSpeakerData)_speakerDataProperty.ValueEntry.WeakSmartValue;
        
            int max = speakerData.portraits.Count - 1;
        
            List<string> names = speakerData.EnumEmotions();
            
            int[] ids = new int[names.Count];
            for (int i = 0; i < names.Count; ++i)
                ids[i] = i;
            if (names.Count != 0)
                value = EditorGUILayout.IntPopup(value,  names.ToArray(), ids);
            
        }

        this.ValueEntry.SmartValue = value;
    }
}
