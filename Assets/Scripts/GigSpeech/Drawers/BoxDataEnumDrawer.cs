using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector.Editor;
using UnityEngine;
using System;
using System.Diagnostics;
using GigSpeech;
using Sirenix.Utilities.Editor;
using UnityEditor;

[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
[Conditional("UNITY_EDITOR")]
public class BoxDataEnumAttribute : Attribute
{

    public BoxDataEnumAttribute()
    {
    }
}

public class BoxDataEnumDrawer : OdinAttributeDrawer<BoxDataEnumAttribute, int>
{

    protected override void Initialize()
    {
    }

    protected override void DrawPropertyLayout(GUIContent label)
    {
        Rect rect = EditorGUILayout.GetControlRect();

        if(label != null)
            rect = EditorGUI.PrefixLabel(rect, label);
        
        
        int value = this.ValueEntry.SmartValue;

        List<GigSpeechBoxData> boxData = GigSpeechManager.Instance.parameters.boxData;
        if(boxData.Count == 0)
            SirenixEditorGUI.ErrorMessageBox("Cannot find any gig speech box data");
    
    
        List<string> names = new List<string>();
        int[] ids = new int[boxData.Count];
        for (int i = 0; i < boxData.Count; ++i)
        {
            names.Add(boxData[i].name);
            ids[i] = i;
        }
        
        if (boxData.Count != 0)
            value = EditorGUILayout.IntPopup(value,  names.ToArray(), ids);

        this.ValueEntry.SmartValue = value;
    }
}

