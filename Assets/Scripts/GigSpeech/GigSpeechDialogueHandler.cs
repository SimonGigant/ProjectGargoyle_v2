﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Sequence = DG.Tweening.Sequence;


namespace GigSpeech
{
    public enum GigSpeechSpeakerPosition { None, Left, Right, _COUNT}
    
    [RequireComponent(typeof(GigSpeechUIShake))]
    public class GigSpeechDialogueHandler : MonoBehaviour
    {
        public GigSpeechDialogueSequence thisGigSpeechDialogue;


        private List<GigSpeechDialogueBitData> _dialogueData;
        private bool _textIsPrinted = false;
        private float _counter = 0.0f;
        private DOTweenTMPAnimator _animator;
        private Sequence _sequence;
        private Sequence _sequencePortraitSpeaker;
        private Sequence _sequencePortraitListener;
        private string _speakerNameL;
        private string _speakerNameR;
        private bool _isTwinkling = false;
        private Coroutine _twinkle;
        private float _scalePortrait;

        private GigSpeechSpeakerPosition _speakerPosition;
        //Cache :
        private bool _portraitShouldTransition = true;
        private bool _currentNoSpeaker = false;
        private bool _rightPortraitEntered = false;
        private bool _leftPortraitEntered = false;


        [Header("Fields")]
        [SerializeField] private TMP_Text text = null;
        [SerializeField] private TMP_Text speakerName = null;
        [SerializeField] private Image portraitR = null;
        [SerializeField] private Image portraitL = null;
        [SerializeField] private Image endDialogueSign = null;
        [SerializeField] private Material shaderNormal = null;
        [SerializeField] private Image box = null;


        private void Start()
        {
            DOTween.SetTweensCapacity(1250,250);
            GetComponentInParent<Canvas>().worldCamera = Camera.main;
            _scalePortrait = 1.0f;

            if (GigSpeechManager.Instance.parameters.boxData[0].neutralBox != null)
            {
                box.sprite = GigSpeechManager.Instance.parameters.boxData[0].neutralBox;
            }

            if (GigSpeechManager.Instance.parameters.showListener)
            {
                switch (_speakerPosition)
                {
                    case GigSpeechSpeakerPosition.None:
                        portraitL.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
                        portraitR.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
                        break;
                    case GigSpeechSpeakerPosition.Left:
                        portraitR.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
                        break;
                    case GigSpeechSpeakerPosition.Right:
                        portraitL.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
                        break;
                }
            }
            else
            {
                //Hide the listener sprite
                switch (_speakerPosition)
                {
                    case GigSpeechSpeakerPosition.Left:
                        SetPortraitAlpha(true, false);
                        break;
                    case GigSpeechSpeakerPosition.Right:
                        SetPortraitAlpha(false, false);
                        break;
                }
            }
            
            if (GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.Basic)
            {
                SetPortraitAlpha(true, false);
                SetPortraitAlpha(false, false);
            }
            StartCoroutine(TimerBeforeBegin(GigSpeechManager.Instance.parameters.durationAnimationEnteringBox));
        }

        /**
         * Wait the duration before starting the dialogue
         */
        private IEnumerator TimerBeforeBegin(float duration)
        {
            transform.DOLocalMoveY(-2000.0f, GigSpeechManager.Instance.parameters.durationAnimationEnteringBox).From().SetEase(Ease.OutQuad);
            StartDialogue(thisGigSpeechDialogue);
            yield return new WaitForSeconds(duration);
            UpdateDisplay();
        }

        /**
         * Wait the duration before removing the bubble
         */
        private IEnumerator TimerBeforeEnd(float duration)
        {
            /*make the player not move*/
            transform.DOLocalMoveY(-2000f, GigSpeechManager.Instance.parameters.durationAnimationEnteringBox).SetEase(Ease.InQuad);
            yield return new WaitForSeconds(duration);
            Destroy(transform.parent.gameObject);
        }

        private void Update()
        {
            _counter += Time.deltaTime;
            if (_textIsPrinted && !_isTwinkling && GigSpeechManager.Instance.parameters.twinklingDotWhenCanPass)
            {
                _twinkle = StartCoroutine(Twinkle());
            }
            if (_textIsPrinted && GigSpeechManager.Instance.parameters.autoPass && _counter > GigSpeechManager.Instance.parameters.autoPassDuration)
            {
                PassDialogue();
            }
        }

    public bool IsPunctuation(char c)
    {
        return c == '.' || c == ',' || c == '?' || c == ';' || c == '!' || c == '-' || c == ':' || c == '/' ||
               c == '(' || c == ')' || c == '—';
    }
        
    /**
     * Translate the tag-based text into the printed text
     * and cache where the different effects will be called
     */
    private GigSpeechDialogueBitData TranslateFx(GigSpeechDialogueBitData bitData)
        {
            GigSpeechDialogueBitData res = new GigSpeechDialogueBitData(bitData)
            {
                emphasis = new List<bool>(),
                size = new List<TextSize>(),
                effect = new List<TextEffect>(),
                speed = new List<TextSpeed>()
            };
            string thisText = "";
            bool isEmphasized = false;
            bool ignoreFormatting = false;
            TextSize currentSize = TextSize.Normal;
            TextEffect currentEffect = TextEffect.None;
            TextSpeed currentSpeed = TextSpeed.Normal;
            TextSpeed previousSpeed = TextSpeed.Normal;
            bool isPause = false;
            foreach(char c in res.text)
            {
                if (!isPause && currentSpeed == TextSpeed.Pause)
                {
                    currentSpeed = previousSpeed;
                }
                if (!ignoreFormatting || c == '\\')
                {
                    switch (c)
                    {
                        case '*':
                        {
                            isEmphasized = !isEmphasized;
                            if (isEmphasized)
                            {
                                thisText += "<b>";
                            }
                            else
                            {
                                thisText += "</b>";
                            }
                            continue;
                        }
                        case '_':
                        {
                            currentSize = currentSize == TextSize.Small ? TextSize.Normal: TextSize.Small;
                            continue;
                        }
                        case '^':
                        {
                            currentSize = currentSize == TextSize.Big ? TextSize.Normal : TextSize.Big;
                            continue;
                        }
                        case '~':
                        {
                            currentEffect = currentEffect == TextEffect.Wave ? TextEffect.None : TextEffect.Wave;
                            continue;
                        }
                        case '°':
                        {
                            currentEffect = currentEffect == TextEffect.Spooky ? TextEffect.None : TextEffect.Spooky;
                            continue;
                        }
                        case '>':
                        {
                            if (isPause)
                                currentSpeed = previousSpeed;
                            if (currentSpeed < TextSpeed._COUNT - 1)
                            {
                                ++currentSpeed;
                            }
                            continue;
                        }
                        case '<':
                        {
                            if (isPause)
                                currentSpeed = previousSpeed;
                            if (currentSpeed > TextSpeed.Pause + 1)
                            {
                                --currentSpeed;
                            }
                            continue;
                        }
                        case '|':
                        {
                            previousSpeed = currentSpeed == TextSpeed.Pause ? TextSpeed.Normal : currentSpeed;
                            currentSpeed = TextSpeed.Pause;
                            isPause = true;
                            continue;
                        }
                        case '\\':
                        {
                            ignoreFormatting = !ignoreFormatting;
                            continue;
                        }
                    }
                }
                
                
                //Only if this is not a balise
                thisText += c;
                res.emphasis.Add(isEmphasized);
                res.size.Add(currentSize);
                res.effect.Add(currentEffect);
                res.speed.Add(currentSpeed);
                res.ignoreFormatting.Add(ignoreFormatting);
                isPause = false;
            }
            res.text = thisText;
            return res;
        } 
        
    /**
     * Initialize the bubble and portrait before the first dialogue
     */
    private void StartDialogue(GigSpeechDialogueSequence d)
    {
        if (d == null)
            return;
        GigSpeechManager.Instance.OnDialogueBegin?.Invoke();
        _dialogueData = new List<GigSpeechDialogueBitData>();
        foreach(GigSpeechDialogueBitData data in d.dialogues)
        {
            _dialogueData.Add(TranslateFx(data));
        }
        
        CheckIfNoSpeaker();

        if (_currentNoSpeaker)
            _speakerPosition = GigSpeechSpeakerPosition.None;
        else
        {
            _speakerPosition = _dialogueData[0].leftSide
                ? GigSpeechSpeakerPosition.Left
                : GigSpeechSpeakerPosition.Right;
        }

        if (_currentNoSpeaker)
        {
            speakerName.text = "";
        }
        else
        {
            speakerName.text = _dialogueData[0].name;
            Material matName = speakerName.fontMaterial;
            matName.SetColor(ShaderUtilities.ID_UnderlayColor, _dialogueData[0].speaker.color);
            speakerName.fontMaterial = matName;
        }
        
        switch(_speakerPosition)
        {
            case GigSpeechSpeakerPosition.None:
                SetUpLeftListenerFromNextData();
                SetUpRightListenerFromNextData();
                break;
            case GigSpeechSpeakerPosition.Left:
                SetUpLeftSpeakerFromCurrentData();
                break;
            case GigSpeechSpeakerPosition.Right:
                SetUpRightSpeakerFromCurrentData();
                break;
        }
        
        //Listener
        if (_dialogueData[0].overrideListener && _dialogueData[0].listenerOverridden!=null)
        {
            switch (_speakerPosition)
            {
                case GigSpeechSpeakerPosition.None:
                    break;
                case GigSpeechSpeakerPosition.Left:
                    SetUpRightListenerFromCurrentData();
                    break;
                case GigSpeechSpeakerPosition.Right:
                    SetUpLeftListenerFromCurrentData();
                    break;
            }
        }
        else
        {
            switch (_speakerPosition)
            {
                case GigSpeechSpeakerPosition.None:
                    break;
                case GigSpeechSpeakerPosition.Left:
                    //search for the next right speaker to set up the listener
                    SetUpRightListenerFromNextData();
                    break;
                case GigSpeechSpeakerPosition.Right:
                    //search for the next left speaker to set up the listener 
                    SetUpLeftListenerFromNextData();
                    break;
            }
        }

        if (portraitL.sprite == null)
        {
            SetPortraitAlpha(false, false);
        }
        if (portraitR.sprite == null)
        {
            SetPortraitAlpha(true, false);
        }
        text.SetText("");
    }
    
    /**
     * Update the bubble to show the next dialogue
     * Trigger the end of the bubble if there is none
     */
    private void UpdateDisplay()
    {
        if (_dialogueData.Count <= 0)
        {
            StartCoroutine(TimerBeforeEnd(GigSpeechManager.Instance.parameters.durationAnimationEnteringBox));
            GigSpeechManager.Instance.OnDialogueEnd?.Invoke();
            return;
        }

        
        CheckIfNoSpeaker();
        
        GigSpeechManager.Instance.OnDialogueLineBegin?.Invoke(_currentNoSpeaker? null : _dialogueData[0].speaker);


        text.SetText(_dialogueData[0].text);
        speakerName.text = _currentNoSpeaker ? "" : _dialogueData[0].name;
        
        if (_currentNoSpeaker)
            _speakerPosition = GigSpeechSpeakerPosition.None;
        else
        {
            _speakerPosition = _dialogueData[0].leftSide
                ? GigSpeechSpeakerPosition.Left
                : GigSpeechSpeakerPosition.Right;
        }

        if (_currentNoSpeaker && GigSpeechManager.Instance.parameters.noSpeakerIsItalic)
        {
            text.SetText("<i>" + text.text + "</i>");
        }

        switch (_speakerPosition)
        {
            case GigSpeechSpeakerPosition.Left:
                _speakerNameL = _dialogueData[0].name;
                break;
            case GigSpeechSpeakerPosition.Right:
                _speakerNameR = _dialogueData[0].name;
                break;
        }

        int currentBoxEffect = _dialogueData[0].boxEffect;
        if(currentBoxEffect >= GigSpeechManager.Instance.parameters.boxData.Count)
        {
            currentBoxEffect = 0;
        }
        
        switch (_speakerPosition)
        {
            case GigSpeechSpeakerPosition.Left:
                box.sprite = GigSpeechManager.Instance.parameters.boxData[currentBoxEffect].leftBox;
                break;
            case GigSpeechSpeakerPosition.Right:
                box.sprite = GigSpeechManager.Instance.parameters.boxData[currentBoxEffect].rightBox;
                break;
            default:
                box.sprite = GigSpeechManager.Instance.parameters.boxData[currentBoxEffect].neutralBox;
                break;
        }

        if (!_currentNoSpeaker)
        {
            Material matName = speakerName.fontMaterial;
            matName.SetColor(ShaderUtilities.ID_UnderlayColor, _dialogueData[0].speaker.color);
            speakerName.fontMaterial = matName;
        }

        switch(_speakerPosition)
        {
            case GigSpeechSpeakerPosition.None:
                SetUpNoSpeakerPortraitData();
                break;
            case GigSpeechSpeakerPosition.Left:
                SetUpLeftSpeakerPortraitData();
                break;
            case GigSpeechSpeakerPosition.Right:
                SetUpRightSpeakerPortraitData();
                break;
        }

        if (portraitL.sprite == null)
        {
            SetPortraitAlpha(false, false);
        }
        if (portraitR.sprite == null)
        {
            SetPortraitAlpha(true, false);
        }
        
        _textIsPrinted = false;
        text.ForceMeshUpdate();
        FxShowTextCharByChar(text);
        text.ForceMeshUpdate();
    }

    private void SetPortraitAlpha(bool isRight, bool newVisible)
    {
        if (newVisible)
        {
            if (isRight)
            {
                Color c = portraitR.color;
                c.a = 1f;
                portraitR.color = c;
            }
            else
            {
                Color c = portraitL.color;
                c.a = 1f;
                portraitL.color = c;
            }
        }
        else
        {
            if (isRight)
            {
                Color c = portraitR.color;
                c.a = 0f;
                portraitR.color = c;
            }
            else
            {
                Color c = portraitL.color;
                c.a = 0f;
                portraitL.color = c;
            }
        }
    }
    
    //When the speaker is on RIGHT
    //Set up the portrait/name data for the speaker
    private void SetUpRightSpeakerFromCurrentData()
    {
        _speakerNameR = _dialogueData[0].name;
        portraitR.sprite = _dialogueData[0].portrait;
        portraitR.rectTransform.pivot = new Vector2(
            _dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width,
            0);
        portraitR.material = shaderNormal;
        portraitR.transform.localScale = _dialogueData[0].mirror
            ? new Vector2(-_scalePortrait, _scalePortrait)
            : new Vector2(_scalePortrait, _scalePortrait);

        //setting the listener sprite to empty, it could be overriden after if there is actually a listener
        portraitL.sprite = null;
    }

    //When the speaker is on LEFT
    //Set up the portrait/name data for the speaker
    private void SetUpLeftSpeakerFromCurrentData()
    {
        _speakerNameL = _dialogueData[0].name;
        portraitL.sprite = _dialogueData[0].portrait;
        portraitL.rectTransform.pivot = new Vector2(
            _dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width,
            0);
        portraitL.material = shaderNormal;
        portraitL.transform.localScale = _dialogueData[0].mirror
            ? new Vector2(-_scalePortrait, _scalePortrait)
            : new Vector2(_scalePortrait, _scalePortrait);

        //setting the listener sprite to empty, it could be overriden after if there is actually a listener
        portraitR.sprite = null;
    }

    //When the speaker is on RIGHT and data has been overridden
    //Set up the portrait/name data for the listener
    private void SetUpLeftListenerFromCurrentData()
    {
        _speakerNameL = _dialogueData[0].name;
        portraitL.sprite = _dialogueData[0].listenerPortrait;
        portraitL.rectTransform.pivot = new Vector2(
            _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
            0);
        portraitL.transform.localScale = _dialogueData[0].listenerMirror
            ? new Vector2(-_scalePortrait, _scalePortrait)
            : new Vector2(_scalePortrait, _scalePortrait);
    }

    //When the speaker is on LEFT and data has been overridden
    //Set up the portrait/name data for the listener
    private void SetUpRightListenerFromCurrentData()
    {
        _speakerNameR = _dialogueData[0].name;
        portraitR.sprite = _dialogueData[0].listenerPortrait;
        portraitR.rectTransform.pivot = new Vector2(
            _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
            0);
        portraitR.transform.localScale = _dialogueData[0].listenerMirror
            ? new Vector2(-_scalePortrait, _scalePortrait)
            : new Vector2(_scalePortrait, _scalePortrait);
    }

    //When the speaker is on RIGHT, and data has not been overridden
    //Set up the portrait/name data for the listener
    private void SetUpLeftListenerFromNextData()
    {
        for (int i = 1; i < _dialogueData.Count; ++i)
        {
            if (_dialogueData[i].leftSide && ( _currentNoSpeaker || _dialogueData[i].speaker.characterName != _dialogueData[0].speaker.characterName))
            {
                _speakerNameL = _dialogueData[i].name;
                portraitL.sprite = _dialogueData[i].portrait;
                portraitL.rectTransform.pivot = new Vector2(
                    _dialogueData[i].portrait.pivot.x / _dialogueData[i].portrait.rect.width,
                    0);
                portraitL.transform.localScale = _dialogueData[i].mirror
                    ? new Vector2(-_scalePortrait, _scalePortrait)
                    : new Vector2(_scalePortrait, _scalePortrait);
                break;
            }
        }
    }

    //When the speaker is on LEFT, and data has not been overridden
    //Set up the portrait/name data for the listener
    private void SetUpRightListenerFromNextData()
    {
        for (int i = 1; i < _dialogueData.Count; ++i)
        {
            if (!_dialogueData[i].leftSide && (_currentNoSpeaker || _dialogueData[i].speaker.characterName != _dialogueData[0].speaker.characterName))
            {
                _speakerNameR = _dialogueData[i].name;
                portraitR.sprite = _dialogueData[i].portrait;
                portraitR.rectTransform.pivot = new Vector2(
                    _dialogueData[i].portrait.pivot.x / _dialogueData[i].portrait.rect.width,
                    0);
                portraitR.transform.localScale = _dialogueData[i].mirror
                    ? new Vector2(-_scalePortrait, _scalePortrait)
                    : new Vector2(_scalePortrait, _scalePortrait);
                break;
            }
        }
    }
    
    //When the speaker is on RIGHT
    //Set up the portrait data for the speaker
    private void SetUpRightSpeakerPortraitData()
    {
        portraitR.sprite = _dialogueData[0].portrait;
        portraitR.rectTransform.pivot =
            new Vector2(_dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width, 0);
        portraitR.material = shaderNormal;
        portraitR.transform.localScale = _dialogueData[0].mirror
            ? new Vector2(-_scalePortrait, _scalePortrait)
            : new Vector2(_scalePortrait, _scalePortrait);
        portraitR.color = Color.white;

        if (GigSpeechManager.Instance.parameters.showListener)
        {
            portraitL.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
            portraitL.material = shaderNormal;

            if (_dialogueData[0].overrideListener && _dialogueData[0].listenerOverridden != null)
            {
                _speakerNameL = _dialogueData[0].listenerOverridden.characterName;
                portraitL.sprite = _dialogueData[0].listenerPortrait;
                portraitL.rectTransform.pivot = new Vector2(
                    _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
                    0);
                portraitL.transform.localScale = _dialogueData[0].listenerMirror
                    ? new Vector2(-_scalePortrait, _scalePortrait)
                    : new Vector2(_scalePortrait, _scalePortrait);
            }
        }
        else
        {
            SetPortraitAlpha(false, false);
        }
    }
    
    //When the speaker is on LEFT
    //Set up the portrait data for the speaker
    private void SetUpLeftSpeakerPortraitData()
    {
        portraitL.sprite = _dialogueData[0].portrait;
        portraitL.rectTransform.pivot =
            new Vector2(_dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width, 0);
        portraitL.material = shaderNormal;
        portraitL.transform.localScale = _dialogueData[0].mirror
            ? new Vector2(-_scalePortrait, _scalePortrait)
            : new Vector2(_scalePortrait, _scalePortrait);
        portraitL.color = Color.white;

        if (GigSpeechManager.Instance.parameters.showListener)
        {
            portraitR.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
            if (_dialogueData[0].overrideListener && _dialogueData[0].listenerOverridden != null)
            {
                _speakerNameR = _dialogueData[0].listenerOverridden.characterName;
                portraitR.sprite = _dialogueData[0].listenerPortrait;
                portraitR.rectTransform.pivot = new Vector2(
                    _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
                    0);
                portraitR.transform.localScale = _dialogueData[0].listenerMirror
                    ? new Vector2(-_scalePortrait, _scalePortrait)
                    : new Vector2(_scalePortrait, _scalePortrait);
            }
        }
        else
        {
            SetPortraitAlpha(true, false);
        }
    }
    
    //When no speaker
    //Set both portraits data as listeners
    private void SetUpNoSpeakerPortraitData()
    {
        if(_rightPortraitEntered &&  (GigSpeechManager.Instance.parameters.showListener || !_dialogueData[0].leftSide))
            portraitR.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
        else
            SetPortraitAlpha(true, false);
        if (_leftPortraitEntered && (GigSpeechManager.Instance.parameters.showListener || _dialogueData[0].leftSide))
            portraitL.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
        else
            SetPortraitAlpha(false, false);
    }


    /**
     * Pass to the next bubble of dialogue
     */
    private void PassDialogue()
    {
        if(_textIsPrinted && _dialogueData.Count > 0)
        {
            if(_twinkle != null)
                StopCoroutine(_twinkle);
            _isTwinkling = false;
            endDialogueSign.enabled = false;
            
            //For transitions
            //[1] is current line
            //[0] is previous line
            //TODO: rework this!
            if(_dialogueData.Count > 1)
            {
                if (GigSpeechManager.Instance.parameters.showListener)
                {
                    if (_dialogueData[1].leftSide)
                    {
                        _portraitShouldTransition = !_leftPortraitEntered 
                                                    && (_dialogueData[0].noSpeaker && !_dialogueData[1].noSpeaker || 
                                                        (_dialogueData[1].speaker != null && _dialogueData[1].speaker.characterName != _speakerNameL));
                    }
                    else
                    {
                        _portraitShouldTransition = !_rightPortraitEntered
                                                    && (_dialogueData[0].noSpeaker && !_dialogueData[1].noSpeaker || 
                                                        (_dialogueData[1].speaker != null && _dialogueData[1].speaker.characterName != _speakerNameR));
                    }
                }
                else
                {
                    _portraitShouldTransition = !_rightPortraitEntered && (_dialogueData[0].noSpeaker || _dialogueData[1].speaker != null && _dialogueData[0].speaker.characterName != _dialogueData[1].speaker.characterName);
                }

                if (_portraitShouldTransition)
                {
                    _leftPortraitEntered = true;
                    _rightPortraitEntered = true;
                }
            }
            
            _dialogueData.RemoveAt(0);
            UpdateDisplay();
        }
        else if (!_textIsPrinted)
        {
            _sequence.Complete();
            _sequencePortraitSpeaker.Complete();
            _sequencePortraitListener.Complete();
            GigSpeechManager.Instance.OnSkipDialogue?.Invoke();
        }
        else
        {
            _textIsPrinted = false;
        }
    }

    /**
     * Subscribe given event that needed to be called after the dialogue
     */
    public void SubscribeToCallbackAfterDialogue(GigSpeechManager.DialogueEventDelegate call)
    {
        GigSpeechManager.Instance.OnDialogueEnd += call;
    }

    /**
     * Unsubscribe given event that needed to be called after the dialogue
     */
    public void UnsubscribeFromCallbackAfterDialogue(GigSpeechManager.DialogueEventDelegate call)
    {
        GigSpeechManager.Instance.OnDialogueEnd -= call;
    }

    /**
     * Call when the player want to pass the dialogue
     */
    private void PassAction(InputAction.CallbackContext ctx)
    {
        //if(!PauseMenu.isPaused)
        PassDialogue();
    }

    //TODO: develop different types of twinkle
    /**
     * Start the twinkling to notify the player that they can pass the dialogue
     */
    private IEnumerator Twinkle()
    {
        _isTwinkling = true;
        for(; ; )
        {
            yield return new WaitForSeconds(0.5f);
            endDialogueSign.enabled = !endDialogueSign.enabled;
        }
    }

    private void CheckIfNoSpeaker()
    {
        _currentNoSpeaker = _dialogueData[0].noSpeaker;

        //To make sure dialogue will still work even if speaker was not set:
        if (!_currentNoSpeaker && _dialogueData[0].speaker == null)
        {
            Debug.LogError(
                "GigSpeech: Speaker was null and it was not set as NoSpeaker. Please set it as NoSpeaker or set the speaker.");
            _currentNoSpeaker = true;
        }
    }

    /**
     * Shake the UI for the bubble
     */
    private void Shake()
    {
        GetComponent<GigSpeechUIShake>().Shake(0.1f, 5);
    }

    private void BigShake()
    {
        GetComponent<GigSpeechUIShake>().Shake(0.5f, 30);
    }

    private void RotateShake()
    {
        GetComponent<GigSpeechUIShake>().RotateShake(0.3f, 5);
    }

    /**
     * Called on the end of the bubble
     */
    private void FinishPrint()
    {
        _textIsPrinted = true;
        _counter = 0.0f;
    }

    /**
     * The actual tweening on the characters and portraits
     * Plan the sequences for this bubble, and use the cached FX values to apply effects on the text
     */
    private void FxShowTextCharByChar(TMP_Text currentText)
    {
        _sequence = DOTween.Sequence().SetAutoKill(true);
        _sequencePortraitSpeaker = DOTween.Sequence().SetAutoKill(true);
        RectTransform portraitAnchor = null;
        RectTransform portraitAnchorListener = null;
        switch (_speakerPosition)
        {
            case GigSpeechSpeakerPosition.Left:
                portraitAnchor = portraitL.GetComponent<RectTransform>();
                portraitAnchorListener = portraitR.GetComponent<RectTransform>();
                break;
            case GigSpeechSpeakerPosition.Right:
                portraitAnchor = portraitR.GetComponent<RectTransform>();
                portraitAnchorListener = portraitL.GetComponent<RectTransform>();
                break;
        }
        _animator ??= new DOTweenTMPAnimator(currentText);

        _sequencePortraitListener = DOTween.Sequence().SetAutoKill(true);

        int start = 0;
        int end = _animator.textInfo.characterCount;
        float offsetCumul = 0.0f;
        char previousChar = '\0';
    
        Color textColor = GigSpeechManager.Instance.parameters.baseColor;
        textColor.a = 0.0f;
        currentText.color = textColor;

        //Big shake at the start of the dialogue bit
        if(GigSpeechManager.Instance.parameters.boxData[_dialogueData[0].boxEffect].shakeWhenSpeak)
        {
            BigShake();
        }
        else
        {
            Shake();
        }

        for (int i = start; i < end; ++i)
        {
            float currentTimeOffset = GigSpeechManager.Instance.parameters.delayBetweenLetters * 0.05f;
            Vector3 currentPosOffset = _animator.GetCharOffset(i);
            float speedMultiplier = 1f;
            Sequence portraitSequence = DOTween.Sequence().SetAutoKill(true);
            Sequence portraitSequenceListener = DOTween.Sequence().SetAutoKill(true);
            Sequence charSequence = DOTween.Sequence().SetAutoKill(true);
            int portraitMoveType = 0;
            char thisChar = _animator.textInfo.characterInfo[i].character;
            UnityEvent onComplete = new UnityEvent();

            //Set speed multiplier depending on the tags
            switch (_dialogueData[0].speed[i])
            {
                case TextSpeed.Pause:
                {
                    speedMultiplier *= 10f;
                    break;
                }
                case TextSpeed.Slow:
                {
                    speedMultiplier *= 3f;
                    break;
                }
                case TextSpeed.Fast:
                {
                    speedMultiplier *= 0.5f;
                    break;
                }
                case TextSpeed.Faster:
                {
                    speedMultiplier *= 0.2f;
                    break;
                }
            }
            currentTimeOffset *= speedMultiplier;


            //Get previous character
            if (i != 0)
                previousChar = _animator.textInfo.characterInfo[i-1].character;
            else
            {
                if(!_currentNoSpeaker)
                    portraitMoveType = -1;
            }

            if (_animator.textInfo.characterInfo[i].isVisible)
            {
                //Set the tween on the text depending on the tag-set size
                switch (_dialogueData[0].size[i])
                {
                    case TextSize.Small:
                    {
                        charSequence.Join(_animator
                                .DOOffsetChar(i,
                                    currentPosOffset + new Vector3(Random.Range(-0.4f, 0.4f),
                                        6f * GigSpeechManager.Instance.parameters.letterAnimationIntensity, 0), 0.1f).From()
                                .SetEase(Ease.OutBack)).OnPlay(onComplete.Invoke)
                            .Join(_animator.DOScaleChar(i, 0.65f, 0f))
                            .Join(_animator.DOColorChar(i, GigSpeechManager.Instance.parameters.colorAparte, 0.01f))
                            .Join(_animator.DOFadeChar(i, 1.0f, 0.01f).SetEase(Ease.OutQuad));
                        currentTimeOffset *= 1.4f;
                        break;
                    }
                    case TextSize.Big:
                    {
                        charSequence.Join(_animator
                                .DOOffsetChar(i,
                                    currentPosOffset + new Vector3(Random.Range(-10f, 10f), 16f * GigSpeechManager.Instance.parameters.letterAnimationIntensity, 0),
                                    0.1f).From()
                                .SetEase(Ease.OutBack)).OnPlay(onComplete.Invoke)
                            .Join(_animator.DOScaleChar(i, 1.25f, 0f))
                            .Join(_animator.DOColorChar(i, GigSpeechManager.Instance.parameters.baseColor, 0.01f))
                            .Join(_animator.DOFadeChar(i, 1.0f, 0.01f).SetEase(Ease.OutQuad));
                        currentTimeOffset *= 1.6f;
                        break;
                    }
                    default:
                    {
                        charSequence.Join(_animator.DOOffsetChar(i, 
                                    currentPosOffset + new Vector3(Random.Range(-10f, 10f), 12f * GigSpeechManager.Instance.parameters.letterAnimationIntensity, 0),
                                    0.1f).From()
                                .SetEase(Ease.OutBack))
                            .OnPlay(onComplete.Invoke)
                            .Join(_animator.DOScaleChar(i, 1f, 0f))
                            .Join(_animator.DOColorChar(i, GigSpeechManager.Instance.parameters.baseColor, 0.01f))
                            .Join(_animator.DOFadeChar(i, 1.0f, 0.01f).SetEase(Ease.OutQuad));
                        break;
                    }
                }
            }
            
            if(GigSpeechManager.Instance.parameters.boxData[_dialogueData[0].boxEffect].shakeWhenSpeak)
            {
                charSequence.OnComplete(Shake);
            }

            //Check punctuation
            if (i != 0 && !_dialogueData[0].ignoreFormatting[i]) {
                //For hard stop
                if ((previousChar == '.'|| previousChar == ':' || previousChar == ';' || previousChar == '?') && !IsPunctuation(thisChar))
                {
                    currentTimeOffset *= 13f;
                }
                //For pause
                else if (previousChar == ',')
                {
                    currentTimeOffset *= 11f;
                }
                //For exclamation
                else if (thisChar == '!' && !IsPunctuation(previousChar) && !_dialogueData[0].emphasis[i])
                {
                    charSequence
                        .Join(_animator.DOScaleChar(i, new Vector3(2.0f, 2.0f, 0.0f), 0.4f).From().SetEase(Ease.OutBounce))
                        .OnStart(Shake);
                    if (portraitMoveType == 0)
                        portraitMoveType = 1;
                }
                //For the end of a small text
                else if (_dialogueData[0].size[i - 1] == TextSize.Small && _dialogueData[0].size[i] != TextSize.Small)
                {
                    currentTimeOffset *= 14f;
                }
                //For after an exclamation
                else if (previousChar == '!')
                {
                    currentTimeOffset *= 13f;
                }
            }

            //Check next char
            if (i < end-1)
            {
                TMP_CharacterInfo nextCharInfo = _animator.textInfo.characterInfo[i + 1];
                char nextChar = nextCharInfo.character;
                //To make the exclamation hit sooner and have more impact
                if (nextChar == '!' && thisChar == ' ')
                    currentTimeOffset *= 0.3f;
                //End of a word
                if (nextChar == ' ')
                {
                    if(portraitMoveType != -1)
                        portraitMoveType = 2;
                }
            }
            //This is the last letter of the text
            else
                portraitMoveType = 2;

            //If it was tagged on emphasis
            if (_dialogueData[0].emphasis[i])
            {
                currentTimeOffset *= 2f;
                //charSequence.Join(_tweener.DOScaleY(i,5f, 0.15f * speedMultiplier).From().SetEase(Ease.InOutQuad)).OnStart(Shake);
                charSequence
                    .Join(_animator.DOScaleChar(i, new Vector3(3, 3, 0), 0.15f * speedMultiplier).From()
                        .SetEase(Ease.OutBack)).OnStart(Shake);
                if (portraitMoveType == 0)
                    portraitMoveType = 1;
            }
            //Special effect: ~Wave~
            if (_dialogueData[0].effect[i] == TextEffect.Wave)
            {
                Tween sine = _animator.DOOffsetChar(i, new Vector3(0.0f,6,0), 0.4f).SetEase(Ease.InOutSine)
                    .SetLoops(3, LoopType.Yoyo).SetAutoKill(true);
                Tween endSine = _animator.DOOffsetChar(i, Vector3.zero, 0.5f).SetEase(Ease.InOutSine).SetAutoKill(true);
                charSequence.Append(sine);
                charSequence.Append(endSine);
            }
            //Special effect: ṡ̵͇̎̕p̴̞̞̰̘̎͒̽͝o̷̹̙̒̀̈́͐o̵̪̪͍̽͂̏͝ͅk̴͍̓y̸̱̣̪͈͍͑̈
            else if (_dialogueData[0].effect[i] == TextEffect.Spooky)
            {
                Sequence spook = DOTween.Sequence().SetAutoKill(true);
                spook.Append(_animator.DOShakeCharOffset(i, 1.0f, 3.0f, 105, 180f));
                charSequence.Append(spook);
            }

            _sequence.Insert(currentTimeOffset + offsetCumul, charSequence);
            
            //Portrait anims
            if (!_currentNoSpeaker)
            {
                //Speaker Anim:
                PortraitAnim(ref portraitSequence, portraitMoveType, portraitAnchor, _dialogueData[0].leftSide ? -1f : 1f);
                _sequencePortraitSpeaker.Insert(currentTimeOffset + offsetCumul, portraitSequence);
                
                //Listener Anim:
                if (GigSpeechManager.Instance.parameters.showListener && portraitMoveType == -1)
                {
                    PortraitAnim(ref portraitSequenceListener, -1, portraitAnchorListener, _dialogueData[0].leftSide ? 1f : -1f, true);
                    _sequencePortraitListener.Insert(currentTimeOffset + offsetCumul + 0.1f, portraitSequenceListener);
                }
            }
            
            offsetCumul += currentTimeOffset;
        }
        _sequence.OnComplete(FinishPrint);
    }
        
        
        
        
        
        
        
    //************************
    // Portrait animations:


        /**
         * Animate the portrait
         * with the given type of move, and the given anchor
         */
        private void PortraitAnim(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor, float direction, bool isListener = false)
        {
            switch (GigSpeechManager.Instance.parameters.transitionType)
            {
                case GigSpeechPortraitTransitionType.Basic:
                {
                    PortraitAnimTransitionBasic(ref portraitSequence, portraitMoveType, portraitAnchor, direction, isListener);
                    break;
                }
            }
        
            switch (GigSpeechManager.Instance.parameters.animType)
            {
                case GigSpeechPortraitAnimType.PuppetRotation:
                {
                    PortraitAnimPuppetRotation(ref portraitSequence, portraitMoveType, portraitAnchor);
                    break;
                }
                case GigSpeechPortraitAnimType.PuppetTranslation:
                {
                    PortraitAnimPuppetTranslation(ref portraitSequence, portraitMoveType, portraitAnchor);
                    break;
                }
                case GigSpeechPortraitAnimType.Scale:
                {
                    PortraitAnimScale(ref portraitSequence, portraitMoveType, portraitAnchor);
                    break;
                }
            }
        }

        /**
         * Portrait animation with rotations around the anchor (like MatPat in Game Theory)
         */
        private void PortraitAnimPuppetRotation(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor)
        {
            //Types are:
            //0: basic
            //1: intense
            //2: end of a word
            switch (portraitMoveType)
            {
                case 1:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.forward * (Random.Range(0f,1f) * 25.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity), 0.03f).SetEase(Ease.OutBounce));
                    break;
                }
                case 2:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.forward * (Random.Range(0f, 1f) * 10.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity), 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.zero, 0.1f).SetEase(Ease.OutBounce));
                    break;
                }
                default:
                {
                    if (portraitMoveType != -1 || GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.None)
                    {
                        portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.forward * (Random.Range(0f, 1f) * 10.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity), 0.03f).SetEase(Ease.OutBounce));
                    }
                    break;
                }
            }
        }

        /**
         * Portrait animation with Y scale (stretch)
         */
        private void PortraitAnimScale(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor)
        {
            switch (portraitMoveType)
            {
                case 1:
                {
                    float scaleTargetY = _scalePortrait * Random.Range(1f - 0.1f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 1f + 0.1f * GigSpeechManager.Instance.parameters.portraitShakeIntensity);
                    portraitSequence.Append(portraitAnchor.DOScaleY(scaleTargetY, 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Join(portraitAnchor.DOScaleX(Mathf.Sign(portraitAnchor.transform.localScale.x) * (2f * _scalePortrait - scaleTargetY), 0.03f).SetEase(Ease.OutBounce));
                    break;
                }
                case 2:
                {
                    float scaleTargetY = _scalePortrait * Random.Range(1f - 0.07f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 1f + 0.07f * GigSpeechManager.Instance.parameters.portraitShakeIntensity);
                    portraitSequence.Append(portraitAnchor.DOScaleY(scaleTargetY, 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Join(portraitAnchor.DOScaleX(Mathf.Sign(portraitAnchor.transform.localScale.x) * (2f * _scalePortrait - scaleTargetY), 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Append(portraitAnchor.DOScaleY(_scalePortrait, 0.1f).SetEase(Ease.OutBounce));
                    portraitSequence.Join(portraitAnchor.DOScaleX(_scalePortrait * Mathf.Sign(portraitAnchor.transform.localScale.x), 0.1f).SetEase(Ease.OutBounce));
                    break;
                }
                default:
                {
                    if (portraitMoveType != -1 || GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.None)
                    {
                        float scaleTargetY = _scalePortrait * Random.Range(1f - 0.05f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 1f + 0.05f * GigSpeechManager.Instance.parameters.portraitShakeIntensity);
                        portraitSequence.Append(portraitAnchor.DOScaleY(scaleTargetY, 0.03f).SetEase(Ease.OutBounce));
                        portraitSequence.Join(portraitAnchor.DOScaleX(Mathf.Sign(portraitAnchor.transform.localScale.x) * (2f * _scalePortrait - scaleTargetY), 0.03f).SetEase(Ease.OutBounce));
                    }
                    break;
                }
            }
        }

        /**
         * Portrait animation with x translations (gliding)
         */
        private void PortraitAnimPuppetTranslation(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor)
        {
            switch (portraitMoveType)
            {
                case 1:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalMoveX(Random.Range(-1f, 1f) * 40.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 0.03f).SetEase(Ease.OutBounce));
                    break;
                }
                case 2:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalMoveX(Random.Range(-1f, 1f) * 20.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Append(portraitAnchor.DOLocalMoveX(0f, 0.1f).SetEase(Ease.OutBounce));
                    break;
                }
                default:
                {
                    if (portraitMoveType != -1 || GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.None)
                    {
                        portraitSequence.Append(portraitAnchor.DOLocalMoveX(Random.Range(-1f, 1f) * Random.Range(0f, 1f) * 20.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 0.03f).SetEase(Ease.OutBounce));
                    }
                    break;
                }
            }
        }

        /**
         * Enter with a glide from outside of the screen
         */
        private void PortraitAnimTransitionBasic(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor, float direction, bool isListener)
        {
            if(portraitMoveType == -1)
            {
                if (_portraitShouldTransition)
                {
                    Vector3 randomRotation = -Vector3.forward * ((Random.value+1.0f) * 10f);
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0f));
                    float target = 15f * direction * 100f * GigSpeechManager.Instance.parameters.portraitShakeIntensity;
                    portraitSequence.Join(portraitAnchor.DOLocalMoveX(0f, 0.2f).SetEase(Ease.OutQuad).From(target, true));
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0.2f).SetEase(Ease.OutBounce).From());
                    switch (_speakerPosition)
                    {
                        case GigSpeechSpeakerPosition.Left:
                            SetPortraitAlpha(false, true);
                            break;
                        case GigSpeechSpeakerPosition.Right:
                            SetPortraitAlpha(true, true);
                            break;
                    }
                }
                else if(!isListener)
                {
                    Vector3 randomRotation = direction * ((Random.value+1.0f) * 10f) * Vector3.forward;
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0f));
                    portraitSequence.AppendInterval(0.4f);
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0.2f).SetEase(Ease.OutBounce).From());
                    switch (_speakerPosition)
                    {
                        case GigSpeechSpeakerPosition.Left:
                            SetPortraitAlpha(false, true);
                            break;
                        case GigSpeechSpeakerPosition.Right:
                            SetPortraitAlpha(true, true);
                            break;
                    }
                }
            }
        }
    }
}
