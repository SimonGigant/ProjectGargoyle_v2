﻿using System;
using UnityEngine;

namespace GigSpeech
{
    [RequireComponent(typeof(GigSpeechConversation))]
    public class GigSpeechConversationSpawn : MonoBehaviour
    {
        private GigSpeechConversation gigSpeechConversation;
        private bool _triggered = false;

        public void StartDialogue()
        {
            if (!_triggered)
            {
                Debug.Log("Trigger");
                _triggered = true;
                gigSpeechConversation.StartConversation();
            }
        }

        public void OnValidate()
        {
            gigSpeechConversation = GetComponent<GigSpeechConversation>();
            if(gigSpeechConversation != null)
            {
                gameObject.name = "Trigger" + gigSpeechConversation.conversationData.name;
            }
        }
    }
}
