using System;
using UnityEngine;

namespace GigSpeech
{
    public class GigSpeechConversation : MonoBehaviour
    {
        public GigSpeechConversationData conversationData;

        //Starts with dialogue tagged with id 0
        [SerializeField] private bool startWithADialogue = true;
        
        //Cache:
        private GigSpeechConversationOptionBase _currentOption;

        private void Start()
        {
            GigSpeechManager.Instance.OnDialogueEnd += OnDialogueSequenceEnd;
        }

        private void OnDestroy()
        {
            if(GigSpeechManager.DoesInstanceExist())
                GigSpeechManager.Instance.OnDialogueEnd -= OnDialogueSequenceEnd;
        }

        public void StartConversation()
        {
            if (startWithADialogue)
            {
                if (Trigger(0))
                    return;
            }
            
            //TODO: Print Options Menu
        }
        
        public bool Trigger(int id)
        {
            if(conversationData == null || conversationData.options == null)
                return false;
            foreach (GigSpeechConversationOptionBase option in conversationData.options)
            {
                switch (option)
                {
                    case null:
                    {
                        break;
                    }
                    case GigSpeechConversationOptionDialogue asDialogue:
                    {
                        if (asDialogue.id == id)
                        {
                            //ftm just reinstantiate every time the dialogue Handler
                            //TODO: add a way for the dialogue handler to manage multiple dialogues + answers

                            _currentOption = asDialogue;
                            
                            GameObject dialogue = Instantiate(GigSpeechManager.Instance.parameters.dialoguePrefab);
                            GigSpeechDialogueHandler dialogueHandler = dialogue.GetComponentInChildren<GigSpeechDialogueHandler>();
                            dialogueHandler.thisGigSpeechDialogue = asDialogue.sequence;
                            return true;
                        }
                        break;
                    }
                }
            }

            return false;
        }

        private void OnDialogueSequenceEnd()
        {
            if (_currentOption != null)
            {
                if (_currentOption is GigSpeechConversationOptionDialogue asDialogue)
                {
                    foreach (string tag in asDialogue.unlockTagAfterPlayingThisDialogue)
                    {
                        GigSpeechProgressionManager.Instance.UnlockTag(tag);
                    }
                }
            }
        }
    }
}
