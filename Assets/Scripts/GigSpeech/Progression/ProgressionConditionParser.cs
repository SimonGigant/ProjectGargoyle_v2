using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using GigSpeech;
using UnityEngine;
using Debug = UnityEngine.Debug;

public abstract class ProgressionConditionParser
{
    
    
    public static ProgressionConditionBase ParseString(string value)
    {
        return ParseNextExpression(ref value);
    }
    
    private static ProgressionConditionBase ParseNextExpression(ref string value)
    {
        if (value == "")
            return null;
        char nextChar = value[0];
        if (nextChar == '(')
        {
            value = value.Remove(0, 1);
            string leftString = ParseToClosingParenthesis(ref value);
            if (value == "")
            {
                return ParseNextExpression(ref leftString);
            }
            char operatorChar = value[0];
            value = value.Remove(0, 1);

            if (!IsOperator(operatorChar))
            {
                PrintError("Unexpected character: " + operatorChar, leftString + operatorChar + value);
                return null;
            }

            return ComputeParsingOperation(ref leftString, operatorChar, ref value);
        }

        if (nextChar == '!')
        {
            char operatorChar = value[0];
            value = value.Remove(0, 1);
            string leftValue = "";
            return ComputeParsingOperation(ref leftValue, operatorChar, ref value);
        }

        if (char.IsLetter(nextChar))
        {
            string leftString = ParseName(ref value);
            if (value == "")
            {
                ProgressionCondition_TagUnlocked res = new ProgressionCondition_TagUnlocked
                {
                    tag = leftString
                };
                return res;
            }

            char operatorChar = value[0];
            value = value.Remove(0, 1);

            if (!IsOperator(operatorChar))
            {
                PrintError("Unexpected character: " + operatorChar, leftString + operatorChar + value);
                return null;
            }

            return ComputeParsingOperation(ref leftString, operatorChar, ref value);
        }

        
        return null;
    }

    private static ProgressionConditionBase ComputeParsingOperation(ref string leftValue, char operatorChar,
        ref string rightValue)
    {
        
        //TODO: priority is right to left atm, need to be left to right:
        // !A&B should be = to (!A)&(B), not to !(A&B)
        ProgressionConditionBase leftResult = ParseNextExpression(ref leftValue);
        ProgressionConditionBase rightResult = ParseNextExpression(ref rightValue);
        switch (operatorChar)
        {
            case '!':
            {
                if (leftResult != null || rightResult == null)
                {
                    PrintError("Operator ! requires a single operand, on its right", leftValue + operatorChar + rightValue);
                    return null;
                }
                
                ProgressionCondition_Not res = new ProgressionCondition_Not
                {
                    subCondition = rightResult
                };
                return res;
                break;
            }
            case '|':
            {
                if (leftResult == null || rightResult == null)
                {
                    PrintError("Operator | requires multiple operands", leftValue + operatorChar + rightValue);
                    return null;
                }
                
                ProgressionCondition_Or res = new ProgressionCondition_Or();
                res.subConditions.Add(leftResult);
                res.subConditions.Add(rightResult);
                return res;
                break;
            }
            case '&':
            {
                if (leftResult == null || rightResult == null)
                {
                    PrintError("Operator & requires multiple operands", leftValue + operatorChar + rightValue);
                    return null;
                }
                ProgressionCondition_And res = new ProgressionCondition_And();
                res.subConditions.Add(leftResult);
                res.subConditions.Add(rightResult);
                return res;
                break;
            }
        }

        return null;
    }
    /**
     * Return content between parenthesis
     * value is the content after the closing parenthesis
     */
    private static string ParseToClosingParenthesis(ref string value)
    {
        int currentIndentation = 1;
        for (int i = 0; i < value.Length; ++i)
        {
            char currChar = value[i];
            if (currChar == '(')
                ++currentIndentation;
            if (currChar == ')')
            {
                --currentIndentation;
                if (currentIndentation == 0)
                {
                    string contentBetweenParenthesis = value.Substring(0, i);
                    value = i == value.Length ? "" : value.Substring(i + 1);
                    return contentBetweenParenthesis;
                }
            }
        }
        PrintError("Can't find closing parenthesis", value);
        return "";
    }

    private static bool IsParenthesis(char value)
    {
        return value is '(' or ')';
    }

    private static bool IsOperator(char value)
    {
        return value is '|' or '&' or '!';
    }
    
    private static bool IsSpecialCharacter(char value)
    {
        return IsParenthesis(value) || IsOperator(value);
    }

    private static string ParseName(ref string value)
    {
        string res;
        for (int i = 0; i < value.Length; ++i)
        {
            if (IsSpecialCharacter(value[i]))
            {
                res = value.Substring(0, i);
                value = value.Substring(i);
                return res;
            }
        }
        res = value;
        value = "";
        return res;
    }

    private static void PrintError(string errorName, string currentContext)
    {
        Debug.LogError("Error while parsing " + currentContext + ": " + errorName);
    }
}
