﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace GigSpeech
{
    public enum TextSize { Small, Normal, Big }
    public enum TextEffect { None, Wave, Spooky }

    public enum TextSpeed { Pause, Slow, Normal, Fast, Faster, _COUNT}

    [System.Serializable]
    public class GigSpeechDialogueBitData
    {
        public GigSpeechDialogueBitData(GigSpeechDialogueBitData d)
        {
            noSpeaker = d.noSpeaker;
            speaker = d.speaker;
            text = d.text;
            emotionID = d.emotionID;
            overloadedName = d.overloadedName;
            overloadName = d.overloadName;
            leftSide = d.leftSide;
            mirror = d.mirror;
            ignoreFormatting = d.ignoreFormatting;
            overrideListener = d.overrideListener;
            listenerOverridden = d.listenerOverridden;
            listenerEmotionID = d.listenerEmotionID;
            listenerMirror = d.listenerMirror;
            boxEffect = d.boxEffect;
        }

        public GigSpeechDialogueBitData()
        {
            noSpeaker = false;
            speaker = null;
            emotionID = 0;
            text = "";
            leftSide = false;
            mirror = false;
            overrideListener = false;
            listenerOverridden = null;
            listenerEmotionID = 0;
            listenerMirror = false;
            boxEffect = 0;
        }

        [ShowInInspector]
        [HorizontalGroup("Portraits")]
        [HideLabel]
        [ReadOnly]
        [Portrait]
        private int _printPortrait;


        //Quick access to values:
        public Sprite portrait => speaker == null || speaker.portraits.Count <= emotionID ? null : speaker.portraits[emotionID].sprite;
        public Sprite listenerPortrait => overrideListener && listenerOverridden!=null && listenerOverridden.portraits.Count > listenerEmotionID ? listenerOverridden.portraits[listenerEmotionID].sprite : null;
        public string name => speaker == null || overloadName ? overloadedName : speaker.characterName;




        //Attributes:
        
        // /!\ When adding any variable, don't forget to change the constructor
        //Speaker:
        [TabGroup("tabSpeakers", "Speaker")]
        [LabelWidth(100)]
        public bool noSpeaker;
        
        [TabGroup("tabSpeakers", "Speaker")]
        public bool overloadName;
        
        [ShowIf("overloadName")]
        [TabGroup("tabSpeakers", "Speaker")]
        public string overloadedName;
        
        [TabGroup("tabSpeakers", "Speaker")]
        [HideIf("noSpeaker")]
        [LabelWidth(100)]
        [AssetSelector(Paths = "Assets/Dialogues/Speakers", FlattenTreeView = true)]
        public GigSpeechSpeakerData speaker;
        
        [TabGroup("tabSpeakers", "Speaker")]
        [HideIf("@this.noSpeaker || speaker == null")]
        [HideLabel]
        [PortraitEmotionEnum("speaker")]
        public int emotionID;
        
        [TabGroup("tabSpeakers", "Speaker")]
        [HideIf("@this.noSpeaker || speaker == null")]
        [LabelWidth(100)]
        public bool mirror;
        
        [TabGroup("tabSpeakers", "Speaker")]
        [HideIf("@this.noSpeaker || speaker == null")]
        [LabelWidth(100)]
        public bool leftSide;
        
        
        //Listener:
        [TabGroup("tabSpeakers", "Listener")]
        [LabelWidth(100)]
        public bool overrideListener;
        
        [TabGroup("tabSpeakers", "Listener")]
        [ShowIf("overrideListener")]
        [LabelWidth(100)]
        public GigSpeechSpeakerData listenerOverridden;
        
        [TabGroup("tabSpeakers", "Listener")]
        [ShowIf("@this.overrideListener && this.listenerOverridden != null")]
        [LabelWidth(100)]
        [PortraitEmotionEnum("listenerOverridden")]
        public int listenerEmotionID;
        
        [TabGroup("tabSpeakers", "Listener")]
        [ShowIf("@this.overrideListener && this.listenerOverridden != null")]
        [LabelWidth(100)]
        public bool listenerMirror;
        
        //Text:
        [TabGroup("tabSpeakers", "BoxEffects")]
        [BoxDataEnum()]
        public int boxEffect;
        
        [HideInInspector]
        public List<GigSpeechEvent> events;
        
        
        
        [DetailedInfoBox("Expand to see formatting",
            "Effects:\n"
                         +"- Emphasize: *\n"
                         +"- Small: _\n"
                         +"- Big: ^\n"
                         +"- Wavy: ~\n"
                         +"- Spooky: °\n"
                         +"- Increase Speed: >\n"
                         +"- Decrease Speed: <\n"
                         +"- Pause Text: |\n"
                         +"- Ignore Formatting: \\\n"
        )]
        [Title("Dialogue line text", bold: false)]
        [HideLabel]
        [MultiLineProperty(4)]
        public string text;
        
        
        
        
        //Computing values:
        [HideInInspector] public List<bool> emphasis;
        [HideInInspector] public List<TextSize> size;
        [HideInInspector] public List<TextEffect> effect;
        [HideInInspector] public List<TextSpeed> speed;
        [HideInInspector] public List<bool> ignoreFormatting;
        [HideInInspector] public List<int> triggerEvent;
    }
}