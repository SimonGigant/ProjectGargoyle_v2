﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace GigSpeech
{

    [System.Serializable]
    public class GigSpeechPortraitData
    {
        
        [LabelWidth(50)]
        [PreviewField(150)]
        [HideLabel]
        [HorizontalGroup("Portrait", 180)]
        [AssetSelector(Paths = "Assets/Art/Characters", FlattenTreeView = true)]
        public Sprite sprite;
        
        [HorizontalGroup("Portrait")]
        [LabelWidth(50)]
        public string name;

        public GigSpeechPortraitData(string name, Sprite sprite)
        {
            this.name = name;
            this.sprite = sprite;
        }
    }

    [CreateAssetMenu(fileName = "New GigSpeechSpeakerData", menuName = "GigSpeech/SpeakerData", order = 51)]
    [System.Serializable]
    public class GigSpeechSpeakerData : ScriptableObject
    {
        public string characterName;
        [ColorPalette]
        public Color color;
        
        
        public List<GigSpeechPortraitData> portraits;

        public GigSpeechSpeakerData()
        {
            portraits = new List<GigSpeechPortraitData>();
        }

        public List<string> EnumEmotions()
        {
            List<string> emotions = new List<string>();
            foreach(GigSpeechPortraitData p in portraits)
            {
                emotions.Add(p.name);
            }
            return emotions;
        }

        public void CreateNewPortraitData()
        {
            portraits.Add(new GigSpeechPortraitData("new emotion", null));
        }

        public void RemovePortraitData(GigSpeechPortraitData p)
        {
            portraits.Remove(p);
        }
    }
    
    
    
    
    
    
    
    
    
    
#if UNITY_EDITOR
    //[CustomEditor(typeof(GigSpeechSpeakerData))]
    class GigSpeechSpeakerDataEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GigSpeechSpeakerData gigSpeechSpeakerData = (GigSpeechSpeakerData)target;
            if (gigSpeechSpeakerData == null) return;
            Undo.RecordObject(gigSpeechSpeakerData, "Change GigSpeechSpeakerData");

            gigSpeechSpeakerData.characterName = EditorGUILayout.TextField("Name:", gigSpeechSpeakerData.characterName);
            gigSpeechSpeakerData.color = EditorGUILayout.ColorField(gigSpeechSpeakerData.color);
            GUILayout.Space(50f);

            foreach(GigSpeechPortraitData portrait in gigSpeechSpeakerData.portraits)
            {
                GUILayout.BeginHorizontal();
                GUILayout.BeginVertical();
                portrait.name = EditorGUILayout.TextField("", portrait.name);
                GUILayout.BeginHorizontal();
                GUILayout.FlexibleSpace();
                if(GUILayout.Button("-", GUILayout.Width(30f)))
                {
                    gigSpeechSpeakerData.RemovePortraitData(portrait);
                }
                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
                portrait.sprite = (Sprite)EditorGUILayout.ObjectField("", portrait.sprite, typeof(Sprite), allowSceneObjects: false);
                GUILayout.EndHorizontal();
            }
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if (GUILayout.Button("+", GUILayout.Width(30f)))
            {
                gigSpeechSpeakerData.CreateNewPortraitData();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(gigSpeechSpeakerData);
            }
        }
    }
#endif
}