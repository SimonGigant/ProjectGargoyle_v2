﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using Sirenix.Utilities.Editor;
using UnityEditor;
using UnityEngine;

namespace GigSpeech
{
    [System.Serializable]
    public class ProgressionConditionHandler
    {
        public string stringCondition; 
            
        [HideInInspector]
        public ProgressionConditionBase rootCondition;

        [Button("Parse String Test")]
        public void ParseStringCondition()
        {
            rootCondition = ProgressionConditionParser.ParseString(stringCondition);
            if(rootCondition != null)
                rootCondition.DebugPrint();
        }

        public bool ComputeCondition()
        {
            if(rootCondition == null)
                ParseStringCondition();
            return rootCondition == null || rootCondition.Compute();
        }
    }
    
    [System.Serializable]
    public abstract class ProgressionConditionBase
    {
        public virtual bool Compute()
        {
            return true;
        }

        public virtual void DebugPrint()
        {
        }
    }

    [System.Serializable]
    public class ProgressionCondition_TagUnlocked : ProgressionConditionBase
    {
        public string tag;
        
        public override bool Compute()
        {
            return GigSpeechProgressionManager.Instance.HasUnlockedTag(tag);
        }

        public override void DebugPrint()
        {
            Debug.Log("TagUnlocked: " + tag);
        }
    }

    [System.Serializable]
    public class ProgressionCondition_Not : ProgressionConditionBase
    {
        public ProgressionConditionBase subCondition;
        
        public override bool Compute()
        {
            return !subCondition.Compute();
        }
        
        public override void DebugPrint()
        {
            Debug.Log("Not(");
            subCondition.DebugPrint();
            Debug.Log(")");
        }
    }

    [System.Serializable]
    public class ProgressionCondition_Or : ProgressionConditionBase
    {
        public ProgressionCondition_Or()
        {
            subConditions = new List<ProgressionConditionBase> {};
        }
        
        public List<ProgressionConditionBase> subConditions;
        public override bool Compute()
        {
            if (subConditions.IsNullOrEmpty())
                return true;

            foreach (ProgressionConditionBase subcond in subConditions)
            {
                if(subcond.Compute())
                    return true;
            }
            return false;
        }

        public override void DebugPrint()
        {
            Debug.Log("Or(");
            foreach (ProgressionConditionBase cond in subConditions)
            {
                cond.DebugPrint();
            }
            Debug.Log(")");
        }
    }
    
    [System.Serializable]
    public class ProgressionCondition_And : ProgressionConditionBase
    {
        public ProgressionCondition_And()
        {
            subConditions = new List<ProgressionConditionBase>
            {};
        }
        
        public List<ProgressionConditionBase> subConditions;

        public override bool Compute()
        {
            if (subConditions.IsNullOrEmpty())
                return true;

            foreach (ProgressionConditionBase subcond in subConditions)
            {
                if(!subcond.Compute())
                    return false;
            }
            return true;
        }

        public override void DebugPrint()
        {
            Debug.Log("And(");
            foreach (ProgressionConditionBase cond in subConditions)
            {
                cond.DebugPrint();
            }
            Debug.Log(")");
        }
    }
}