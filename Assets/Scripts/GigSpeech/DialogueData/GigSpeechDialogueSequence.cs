﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace GigSpeech
{
    [CreateAssetMenu(fileName = "New Dialogue Sequence", menuName = "GigSpeech/DialogueSequence", order = 51)]
    [System.Serializable]
    public class GigSpeechDialogueSequence : ScriptableObject
    {

        [ListDrawerSettings(NumberOfItemsPerPage = 1)]
        public List<GigSpeechDialogueBitData> dialogues;
    }
}