﻿using System;
using UnityEngine;

namespace GigSpeech
{
    public class GigSpeechDialogueSpawn : MonoBehaviour
    {
        public GigSpeechDialogueSequence gigSpeechDialogueSequence;
        private bool _triggered = false;

        public void StartDialogue()
        {
            if (!_triggered)
            {
                _triggered = true;
                GameObject dialogue = Instantiate(GigSpeechManager.Instance.parameters.dialoguePrefab);
                GigSpeechDialogueHandler dialogueHandler = dialogue.GetComponentInChildren<GigSpeechDialogueHandler>();
                dialogueHandler.thisGigSpeechDialogue = gigSpeechDialogueSequence;
            }
        }

        public void OnValidate()
        {
            if(gigSpeechDialogueSequence != null)
            {
                gameObject.name = "Trigger" + gigSpeechDialogueSequence.name;
            }
        }
    }
}
