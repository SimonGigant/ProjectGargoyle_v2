
using PGS.Input;

namespace PGS.GP.Player
{
    public class MoveAction : IPlayerAction
    {
        public UnityEngine.Vector2 Direction;

        public void Execute(UnityEngine.GameObject gameObject)
        {
            var map = InputMapUtils.GetPlayerMap();
            var isRunning = InputUtils.IsPressed(map, MyPlayerInput.Run);

            // tood(sb): Cache Player locomotion and other useful player script in a data class as an Execute function paramter
            var playerLoco = gameObject.GetComponentInChildren<PlayerLocomotion>();

            if (!playerLoco.physicSim.isGrounded && PitonUtils.IsAttached(playerLoco))
            {
                if (PitonUtils.IsUnderRope(playerLoco))
                {
                    LocomotionUtils.ProcessHangingMovement(playerLoco, Direction, UnityEngine.Time.deltaTime);
                    return;
                }
            }
            else
            {
                playerLoco.playerPiton.deltaY = 0f;
            }

            if (playerLoco.playerClimb.IsClimbing)
            {
                LocomotionUtils.ProcessClimbMovement(playerLoco, Direction);
                return;
            }

            LocomotionUtils.ProcessMoveAction(Direction, isRunning, playerLoco);
        }
    }

    public class JumpAction : IPlayerAction
    {
        public UnityEngine.Vector2 Direction;

        public void Execute(UnityEngine.GameObject gameObject)
        {
            var playerLoco = gameObject.GetComponentInChildren<PlayerLocomotion>();
            if (PitonUtils.IsAttached(playerLoco) && PitonUtils.IsNearPiton(playerLoco))
            {
                PitonUtils.DetachPiton(playerLoco);
                return;
            }

            LocomotionUtils.ProcessJumpAction(playerLoco);
        }
    }

    public class ClimbAction : IPlayerAction
    {
        public void Execute(UnityEngine.GameObject gameObject)
        {
            var playerLoco = gameObject.GetComponentInChildren<PlayerLocomotion>();
            LocomotionUtils.ProcessClimbAction(playerLoco);
        }
    }

    public class StopClimbAction : IPlayerAction
    {
        public void Execute(UnityEngine.GameObject gameObject)
        {
            var playerLoco = gameObject.GetComponentInChildren<PlayerLocomotion>();
            LocomotionUtils.ProcessStopClimbAction(playerLoco);
        }
    }

    public class UsePiton : IPlayerAction
    {
        public void Execute(UnityEngine.GameObject gameObject)
        {
            var playerLoco = gameObject.GetComponentInChildren<PlayerLocomotion>();
            PitonUtils.UsePiton(playerLoco);
        }
    }



    public class LandAction : IPlayerAction
    {
        public void Execute(UnityEngine.GameObject gameObject)
        {
            gameObject.GetComponentInChildren<PlayerLocomotion>().Land();
        }
    }
}
