using System;
using System.Collections;
using System.Collections.Generic;
using _3C;
using UnityEngine;
using Random = UnityEngine.Random;

public class Rope : MonoBehaviour
{
    private LineRenderer _renderer;
    public Transform ropeStart;
    private PlayerCharacter _player;
    public float maxRopeLength = 20.0f;
    //[SerializeField] private float currentRopeLength = 0.0f;
    private bool _isShaking;

    private void Awake()
    {
        _renderer = GetComponentInChildren<LineRenderer>();
        _player = GetComponentInParent<PlayerCharacter>(); 
    }

    public float GetRopeLengthLastSection()
    {
        if (_player.attachedToPitons.Count < 1)
            return 0.0f;
        Vector2 from = _player.attachedToPitons[0].transform.position;
        Vector2 to = _player.center.position;
        return Vector2.Distance(to,from);
    }
    
    private float ComputeRopeLengthBetweenPitons()
    {
        float length = 0.0f;
        if (_player.attachedToPitons.Count <= 1)
            return length;
        int i = 1;
        Vector2 prevPitonPosition = _player.attachedToPitons[0].transform.position;
        for (; i < _player.attachedToPitons.Count; ++i)
        {
            Vector2 currPitonPosition = _player.attachedToPitons[i].transform.position;
            length += Vector2.Distance(prevPitonPosition, currPitonPosition);
            prevPitonPosition = currPitonPosition;
        }
        return length;
    }

    /*[ContextMenu("ShortenRope")]
    public void ShortenRope()
    {
        float distOfRope = currentRopeLength - ComputeRopeLengthBetweenPitons();
        if (distOfRope > 1.0f)
        {
            currentRopeLength -= 1.0f;
            distOfRope = currentRopeLength - ComputeRopeLengthBetweenPitons();
            ropeInstance.ChangeRopeLength(distOfRope, _player.Rigidbody2D);
        }
    }
    
    
    [ContextMenu("LengthenRope")]
    public void LengthenRope()
    {
        float distOfRope = currentRopeLength - ComputeRopeLengthBetweenPitons();
        Debug.Log("current near : " + distOfRope + " max near : " + (maxRopeLength - ComputeRopeLengthBetweenPitons()));
        Debug.Log("current : " + currentRopeLength + " max : " + maxRopeLength);
        if (currentRopeLength < maxRopeLength)
        {
            currentRopeLength += 1.0f;
            distOfRope = currentRopeLength - ComputeRopeLengthBetweenPitons();
            ropeInstance.ChangeRopeLength(distOfRope, _player.Rigidbody2D);
        }
    }*/

    public Vector2 ComputeMaxPositionThroughRope(Vector2 positionTest)
    {
        if (_player == null || _player.attachedToPitons.Count == 0)
            return positionTest;
        /*ComputeRopeLength(out float distanceWithoutPlayer);

        Vector2 ropeStartLocalPos = ropeStart.position - _player.transform.position;
        
        float distanceToPosition = Vector2.Distance(_player.AttachedToPitons[_player.AttachedToPitons.Count - 1].transform.position, positionTest + ropeStartLocalPos);
        if (distanceWithoutPlayer + distanceToPosition > maxRopeLength)
        {
            float availableLength = maxRopeLength - distanceWithoutPlayer;
            Vector2 lastPitonRopePosition =
                new Vector2(_player.AttachedToPitons[_player.AttachedToPitons.Count - 1].transform.position.x, _player.AttachedToPitons[_player.AttachedToPitons.Count - 1].transform.position.y) - ropeStartLocalPos;
            Vector2 direction = (positionTest-lastPitonRopePosition).normalized;
            Vector2 bestPosition = lastPitonRopePosition + direction * availableLength;
            return bestPosition;
        }*/

        return positionTest;
    }

    private void Update()
    {
        if (_player.attachedToPitons.Count == 0)
        {
            _renderer.enabled = false;
            return;
        }
        _renderer.enabled = true;
        _renderer.positionCount = _player.attachedToPitons.Count + 1;
        int i = 0;
        for (; i < _player.attachedToPitons.Count; ++i)
        {
            Vector2 pitonPosition = _player.attachedToPitons[i].transform.position;
            _renderer.SetPosition(i, pitonPosition);
        }
        Vector2 playerPosition = ropeStart.position;
        if (_isShaking)
            playerPosition += Random.insideUnitCircle * 0.2f;
        _renderer.SetPosition(i, playerPosition);
    }
    
    public void StartShake()
    {
        if (!_isShaking)
        {
            _isShaking = true;
        }
    }

    public void StopShake()
    {
        if (_isShaking)
        {
            _isShaking = false;
        }
    }
}
