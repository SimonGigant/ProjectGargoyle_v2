using System;
using PGS.GP.Player;
using UnityEngine;

public static class PitonUtils
{
    public static void UsePiton(PlayerLocomotion loco)
    {
        var playerPos = loco.transform.position.xy();
        var layer = LayerMask.GetMask(ClimbUtils.Climbable);
        RaycastHit2D rc = Physics2D.Raycast(loco.center.position.xy() - loco.Direction * 0.1f, loco.Direction, 2.3f, layer);

        ShapeUtils.DrawPoint(rc.centroid, 0.2f);
        UnityEngine.Debug.DrawRay(loco.center.position.xy() - loco.Direction * 0.1f, loco.Direction * 2.3f, Color.cyan, 1f);

        if (rc.collider == null)
            return;

        var piton = loco.playerPiton.GetUnusedPiton();
        if (piton == null)
            return;

        piton.transform.position = rc.point;
        piton.transform.rotation = Quaternion.LookRotation(rc.normal, Vector3.up);

        loco.playerPiton.isAttached = true;

        if (loco.playerPiton.currentAttachedPiton != null)
        {
            var lineRendererCount = loco.playerPiton.currentAttachedPiton.lineRenderer.positionCount;
            loco.playerPiton.currentAttachedPiton.lineRenderer.SetPosition(lineRendererCount - 1, piton.transform.position);
            piton.PreviousPiton = loco.playerPiton.currentAttachedPiton;
        }

        loco.playerPiton.currentAttachedPiton = piton;
        loco.playerPiton.deltaY = 0f;
    }

    public static bool IsAttached(PlayerLocomotion loco)
    {
        return loco.playerPiton.currentAttachedPiton != null;
    }

    public static bool IsUnderRope(PlayerLocomotion loco)
    {
        var playerPos = loco.transform.position.xy();
        var lastRopePoint = loco.playerPiton.GetLastRopePoint().xy();

        return playerPos.y < lastRopePoint.y;
    }

    public static bool IsNearPiton(PlayerLocomotion loco)
    {
        var playerPos = loco.transform.position.xy();
        var lastRopePoint = loco.playerPiton.currentAttachedPiton.transform.position.xy();

        return (playerPos - lastRopePoint).magnitude <= 1f;
    }

    public static void DetachPiton(PlayerLocomotion loco)
    {
        loco.playerPiton.currentAttachedPiton.isUsed = false;
        loco.playerPiton.currentAttachedPiton.Points.Clear();
        loco.playerPiton.currentAttachedPiton.lineRenderer.positionCount = 0;
        loco.playerPiton.deltaY = 0f;

        if (loco.playerPiton.currentAttachedPiton.PreviousPiton != null)
        {
            var previousPiton = loco.playerPiton.currentAttachedPiton.PreviousPiton;
            loco.playerPiton.currentAttachedPiton = previousPiton;

        }
        else
        {
            loco.playerPiton.isAttached = false;
            loco.playerPiton.currentAttachedPiton = null;
        }
    }
}