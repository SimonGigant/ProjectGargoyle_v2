namespace PGS.GP.Player
{
    using System.Collections.Generic;
    using UnityEngine;

    public class PitonData : Game.Component
    {
        public SpriteRenderer m_Highlight;

        public bool isUsed;
        public List<Vector2> Points = new List<Vector2>(1);

        public LineRenderer lineRenderer;
        public PitonData PreviousPiton;


        public override void UpdateComponent(float dt)
        {
            if (Points.Count == 0)
            {
                Points.Add(transform.position);
                return;
            }

            Points[0] = transform.position;

            for (int i = 1; i < Points.Count; i++)
            {
                Vector2 item = Points[i];
                ShapeUtils.DrawPoint(item, 0.2f);
            }

            ShapeUtils.DrawPoint(Points[0], Color.red, 0.2f);
        }
    }
}