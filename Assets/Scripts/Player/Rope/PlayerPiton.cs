namespace PGS.GP.Player
{
    using System.Collections.Generic;
    using UnityEngine;

    public class PlayerPiton : Game.Component
    {
        public int PitonCount;
        public GameObject prefabPitonThrown;
        public bool isAttached;
        public float maxRopeLength = 10f;
        public float deltaY = 0f;

        [HideInInspector]
        public DistanceJoint2D joint;

        [HideInInspector]
        public List<PitonData> pitons = new List<PitonData>();

        public PitonData currentAttachedPiton = null;

        private PlayerLocomotion playerLoco;
        private List<Vector2> Points = new List<Vector2>(1);

        public float Speed = 2f;

        public override void OnStart()
        {
            base.OnStart();
            this.playerLoco = GetComponent<PlayerLocomotion>();
            this.joint = GetComponent<DistanceJoint2D>();

            for (int i = 0; i < PitonCount; i++)
            {
                var newGo = GameObject.Instantiate(prefabPitonThrown);
                pitons.Add(newGo.GetComponent<PitonData>());
            }
        }

        public override void UpdateComponent(float dt)
        {
            joint.enabled = false;
            if (currentAttachedPiton == null)
                return;

            DrawLineRenderer();

            var ropeLength = GetRopeLength();
            var playerPos = playerLoco.transform.position;

            var pitonPos = GetLastRopePoint();
            var ropeLengthMinus = GetRopeLength(1);

            var maxDist = this.maxRopeLength - ropeLengthMinus;

            joint.connectedAnchor = pitonPos;
            joint.enabled = true;
            joint.distance = Mathf.Clamp(maxDist + deltaY, 0, maxDist);

            if (joint.distance == maxDist)
            {
                deltaY = 0f;
            }
        }

        public Vector3 GetLastRopePoint()
        {
            if (currentAttachedPiton.Points.Count == 0)
                return Vector3.zero;

            return currentAttachedPiton.Points[currentAttachedPiton.Points.Count - 1];
        }

        public float GetRopeLength(int minus = 0)
        {
            float dist = 0f;
            for (int i = 1; i < currentAttachedPiton.lineRenderer.positionCount - minus; i++)
            {
                var point = currentAttachedPiton.lineRenderer.GetPosition(i);
                var previousPoint = currentAttachedPiton.lineRenderer.GetPosition(i - 1);

                dist += (previousPoint - point).magnitude;
            }

            return dist;
        }

        public void DrawLineRenderer()
        {
            var pitonPos = currentAttachedPiton.Points[currentAttachedPiton.Points.Count - 1];
            var playerPos = playerLoco.transform.position.xy();
            var toRopePos = (pitonPos - playerPos).normalized;
            var toRopeDist = (pitonPos - playerPos).magnitude;

            var layer = LayerMask.GetMask(ClimbUtils.Climbable);
            var rc = Physics2D.Raycast(playerPos, toRopePos, toRopeDist, layer);
            if (rc.collider != null)
            {
                var newPoint = rc.point;
                if ((newPoint - pitonPos).magnitude >= 0.2f)
                {
                    currentAttachedPiton.Points.Add(newPoint);
                }
            }


            // Remove point if see it from the player
            if (currentAttachedPiton.Points.Count >= 2)
            {
                var previousPitonPos = currentAttachedPiton.Points[currentAttachedPiton.Points.Count - 2];
                var toPreviousRopePos = (previousPitonPos - playerPos).normalized;
                var toPreviousRopeDist = (previousPitonPos - playerPos).magnitude;

                var cast = Physics2D.Raycast(playerPos, toPreviousRopePos, toPreviousRopeDist, layer);
                if (cast.collider != null)
                {
                    var newPoint = cast.point;
                    if ((newPoint - previousPitonPos).magnitude < 0.2f)
                    {
                        currentAttachedPiton.Points.RemoveAt(currentAttachedPiton.Points.Count - 1);
                    }
                }
                else
                {
                    currentAttachedPiton.Points.RemoveAt(currentAttachedPiton.Points.Count - 1);
                }
            }


            currentAttachedPiton.lineRenderer.positionCount = currentAttachedPiton.Points.Count + 1;
            for (int i = 0; i < currentAttachedPiton.Points.Count; i++)
            {
                currentAttachedPiton.lineRenderer.SetPosition(i, currentAttachedPiton.Points[i]);
            }

            currentAttachedPiton.lineRenderer.SetPosition(currentAttachedPiton.Points.Count, playerPos);
        }

        public PitonData GetUnusedPiton()
        {
            foreach (var item in pitons)
            {
                if (!item.isUsed)
                {
                    item.isUsed = true;
                    return item;
                }
            }

            return null;
        }
    }
}