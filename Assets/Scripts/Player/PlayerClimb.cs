namespace PGS.GP.Player
{
    using UnityEngine;

    public class PlayerClimb : Game.Component
    {
        public bool IsClimbing;
        public float WallOffset = 0.5f;
        public float ClimbSpeed = 3f;
        public Collider2D ClimbCheck;
        private PlayerLocomotion playerLoco;

        public override void OnStart()
        {
            base.OnStart();
            this.playerLoco = GetComponent<PlayerLocomotion>();
        }

        public void StopClimb()
        {
            this.IsClimbing = false;
            LocomotionUtils.ResetGravity(this.playerLoco);
        }
    }
}