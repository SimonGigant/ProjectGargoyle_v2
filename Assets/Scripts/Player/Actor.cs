public interface IPlayerActor : IActor
{
}


public interface IPlayerAction : IAction
{
}


public interface IAction
{
    void Execute(UnityEngine.GameObject gameObject);
}

public interface IActor
{
    void Execute(IAction action);
}