namespace PGS.GP.Player
{
    public class PlayerStamina : Game.Component
    {
        public float MaxStamina = 9.0f;

        private float CurrentStamina;
        private bool UseStamina;

        public virtual void OnStart()
        {
            CurrentStamina = MaxStamina;
        }
    }
}