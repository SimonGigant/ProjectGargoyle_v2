using System;
using UnityEngine;

namespace _3C
{
    public class ThrownPiton : MonoBehaviour
    {
        private Collider2D _collider;
        [SerializeField] private float Speed;
        [SerializeField] private GameObject PrefabWhenHit;
        [SerializeField] private LayerMask Layers;
        public PlayerCharacter player;
        private bool triggered = false;
        
        private void Start()
        {
            _collider = GetComponent<Collider2D>();
        }

        private void FixedUpdate()
        {
            Transform trans = transform;
            trans.position += trans.up * (Time.fixedDeltaTime * Speed);
        }

        private void FixPiton()
        {
            triggered = true;
            Transform trans = transform;
            GameObject attachedGameObject = GameObject.Instantiate(PrefabWhenHit, trans.position, trans.rotation);
            Gamefeel.Instance.InitScreenshake(0.15f,0.3f);
            attachedGameObject.GetComponent<Piton>().player = player;
            
            Destroy(gameObject);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (triggered)
                return;
            if(Layers == (Layers | (1 << other.gameObject.layer)))
            {
                FixPiton();
            }
        }
    }
}
