using System;
using System.Collections;
using System.Collections.Generic;
using PowerTools;
using UnityEngine;

public enum PlayerAnimState
{
    Idle,
    Walk,
    Run,
    Climb,
    Hanging
};

public class PlayerAnimations : MonoBehaviour
{
    [SerializeField] private SpriteAnim Animator;

    [SerializeField] private AnimationClip Idle;
    [SerializeField] private AnimationClip Run;
    [HideInInspector] public float ParamSpeed;
    public bool ParamGrounded;
    private PlayerAnimState _animState;

    public void Update()
    {
        if (_animState == PlayerAnimState.Run || _animState == PlayerAnimState.Walk)
        {
            Animator.Play(ParamSpeed > 0.1f ? Run : Idle);
        }
    }

    public void SetAnimation(PlayerAnimState state)
    {
        _animState = state;
        
        switch (state)
        {
            case PlayerAnimState.Climb:
            case PlayerAnimState.Hanging:
            case PlayerAnimState.Idle:
                Animator.Play(Idle);
                break;
            case PlayerAnimState.Run:
                Animator.Play(Run);
                break;
            case PlayerAnimState.Walk:
            {
                Animator.Play(Run);
                break;
            }
        }
    }
}
