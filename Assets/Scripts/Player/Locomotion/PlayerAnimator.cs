
namespace PGS.GP.Player
{
    public class PlayerAnimator : Game.Component
    {
        private MovementState previousState = MovementState.Unknown;
        private PlayerLocomotion PlayerLocomotion;
        private PlayerAnimations animations;

        public override void OnStart()
        {
            PlayerLocomotion = GetComponent<PlayerLocomotion>();
            animations = GetComponentInChildren<PlayerAnimations>();
        }

        public override void UpdateComponent(float dt)
        {
            if (PlayerLocomotion.movementState != previousState)
            {
                previousState = PlayerLocomotion.movementState;

                switch (previousState)
                {
                    case MovementState.Idle:
                        animations.SetAnimation(PlayerAnimState.Idle);
                        break;
                    case MovementState.Run:
                    case MovementState.Walk:
                        animations.SetAnimation(PlayerAnimState.Run);
                        break;
                    case MovementState.Climb:
                        animations.SetAnimation(PlayerAnimState.Climb);
                        break;
                    case MovementState.Hanging:
                        animations.SetAnimation(PlayerAnimState.Hanging);
                        break;
                }
            }
        }
    }
}
