namespace PGS.GP.Player
{
    using System.Collections.Generic;
    using UnityEngine;

    public static class ClimbUtils
    {
        public const string Climbable = "Climbable";
        public static List<Collider2D> overlapCollider = new List<Collider2D>();
        private static ContactFilter2D filter = default(ContactFilter2D);


        public static void Climb(PlayerLocomotion playerLocomotion, float dt)
        {
            var climb = playerLocomotion.playerClimb;
            var box = climb.ClimbCheck;
            var climbableLayer = LayerMask.GetMask(Climbable);
            filter.SetLayerMask(climbableLayer);
            var count = box.OverlapCollider(filter, overlapCollider);

            if (count == 0)
            {
                LocomotionUtils.ProcessStopClimbAction(playerLocomotion);
                return;
            }

            var collider = overlapCollider[0];

            if (!climb.IsClimbing)
            {
                OnStartClimb(playerLocomotion, collider, climb);
                return;
            }

            ClimbMove(playerLocomotion, collider, dt);
        }

        private static void ClimbMove(PlayerLocomotion playerLocomotion, Collider2D collider, float dt)
        {
            var inputDir = playerLocomotion.Direction.y;
            var closestPoint = collider.ClosestPoint(playerLocomotion.gameObject.transform.position);
            var playerPos = playerLocomotion.gameObject.transform.position.xy();
            var dirToPoint = (closestPoint - playerPos).normalized;

            // todo(sb) : Opti
            List<RaycastHit2D> results = new List<RaycastHit2D>();
            var countClimb = UnityEngine.Physics2D.Raycast(playerPos, dirToPoint, filter, results);
            if (countClimb <= 0)
            {
                return;
            }

            var upDir = ComputeUpDirection(playerLocomotion, results[0]);
            playerLocomotion.transform.position += upDir * inputDir * dt * playerLocomotion.playerClimb.ClimbSpeed;
        }

        private static Vector3 ComputeUpDirection(PlayerLocomotion playerLocomotion, RaycastHit2D hitPoint)
        {
            var pointNormal = hitPoint.normal;
            var upDir = Vector3.Cross(pointNormal, Vector3.forward);
            if (!playerLocomotion.facingRight)
            {
                upDir = -upDir;
            }

            return upDir;
        }

        private static void OnStartClimb(PlayerLocomotion playerLocomotion, Collider2D collider, PlayerClimb climb)
        {
            climb.IsClimbing = true;

            var closestPoint = collider.ClosestPoint(playerLocomotion.gameObject.transform.position);
            Vector2 playerPos = playerLocomotion.transform.position;
            var toPoint = (closestPoint - playerPos).normalized;
            playerLocomotion.transform.position = closestPoint - toPoint * climb.WallOffset;

            playerLocomotion.rb.gravityScale = 0f;
            playerLocomotion.rb.velocity = Vector2.zero;
        }
    }
}