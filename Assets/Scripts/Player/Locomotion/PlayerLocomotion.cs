namespace PGS.GP.Player
{
    using System;
    using UnityEngine;

    public class PlayerLocomotion : Game.Component
    {
        public float WalkSpeed = 5;
        public float RunSpeed = 20;
        public float Jumpforce = 10f;
        public float MaxSlopeAngle = 40f;
        [SerializeField]
        private LayerMask whatIsGround;
        public Vector2 Direction;
        public Transform center;

        [HideInInspector]
        public Rigidbody2D rb;
        public float currentSpeed;
        [HideInInspector]
        public PlayerAnimations animations;
        [HideInInspector]
        public MovementState movementState;
        private PlayerAnimations playerAnimation;
        [HideInInspector]
        public PhysicSim physicSim;
        [HideInInspector]
        public PlayerClimb playerClimb;
        [HideInInspector]
        public PlayerStamina playerStamina;
        [HideInInspector]
        public PlayerPiton playerPiton;
        private SpriteRenderer renderer;
        [HideInInspector]
        public bool facingRight;
        [HideInInspector]
        public bool isJumping;

        public const float Smoothing = .05f;

        public MotionComponent motionComponent;

        public override void OnStart()
        {
            rb = GetComponent<Rigidbody2D>();
            playerAnimation = GetComponent<PlayerAnimations>();
            physicSim = GetComponent<PhysicSim>();
            animations = GetComponent<PlayerAnimations>();
            renderer = GetComponentInChildren<SpriteRenderer>();
            playerStamina = GetComponentInChildren<PlayerStamina>();
            playerClimb = GetComponentInChildren<PlayerClimb>();
            playerPiton = GetComponentInChildren<PlayerPiton>();
            motionComponent = GetComponentInChildren<MotionComponent>();
        }

        public void Land()
        {
            isJumping = false;
        }

        public override void UpdateComponent(float dt)
        {
            switch (movementState)
            {
                case MovementState.Idle:
                    Idle(dt);
                    break;
                case MovementState.Walk:
                    Walk(dt);
                    break;
                case MovementState.Run:
                    Run(dt);
                    break;
                case MovementState.Hanging:
                    break;
                case MovementState.Climb:
                    ClimbUtils.Climb(this, dt);
                    break;
            }

            CheckForFlip(Direction.x);
            SlopeNormal();
        }

        public Vector3 SlopeNormal()
        {
            var slopeHit = Physics2D.Raycast(transform.position, Vector2.down, 1f, whatIsGround);
            if (slopeHit.collider != null)
            {
                return slopeHit.normal;
            }

            return Vector2.up;
        }


        public float GetCurrentMovementSpeed()
        {
            switch (movementState)
            {
                case MovementState.Walk:
                    return WalkSpeed;
                case MovementState.Run:
                    return RunSpeed;
            }

            return 0f;
        }

        public bool IsJumping()
        {
            return this.isJumping;
        }

        private void Idle(float dt)
        {
            LocomotionUtils.Move(this, 0f, dt);
        }

        private void Walk(float dt)
        {
            LocomotionUtils.Move(this, this.WalkSpeed, dt);
        }

        private void Run(float dt)
        {
            LocomotionUtils.Move(this, this.RunSpeed, dt);
        }

        private void CheckForFlip(float valueOrientation)
        {
            if (valueOrientation > 0 && !facingRight)
            {
                Flip();
            }
            else if (valueOrientation < 0 && facingRight)
            {
                Flip();
            }
        }

        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            facingRight = !facingRight;

            // Multiply the player's x local scale by -1.
            var transform = renderer.transform;
            var theScale = transform.localScale;
            theScale.x *= -1;
            transform.localScale = theScale;
        }
    }
}