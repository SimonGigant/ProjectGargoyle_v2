namespace PGS.GP.Player
{
    using System;
    using UnityEngine;

    public static class LocomotionUtils
    {
        public static void Move(PlayerLocomotion loco, float speed, float dt)
        {
            var runValue = loco.Direction.x;
            loco.animations.ParamSpeed = runValue;

            loco.currentSpeed = runValue * speed;

            var velocity = loco.rb.velocity;
            var targetVelocity = new Vector2(loco.currentSpeed, 0);

            var groundedIdle = loco.physicSim.isGrounded && loco.currentSpeed == 0f && targetVelocity.y <= 0;
            if (groundedIdle)
                targetVelocity = Vector2.zero;

            var slopeRotation = Quaternion.FromToRotation(Vector3.up, loco.SlopeNormal());
            var adjustedVelocity = slopeRotation * targetVelocity * dt;

            UnityEngine.Debug.DrawRay(loco.transform.position, adjustedVelocity, Color.cyan, dt);

            // And then smoothing it out and applying it to the character
            loco.motionComponent.Add(adjustedVelocity);
            loco.rb.velocity *= Vector2.up;
        }

        public static void ResetGravity(PlayerLocomotion playerLocomotion)
        {
            playerLocomotion.rb.gravityScale = 1f;
        }

        public static void ProcessMoveAction(Vector2 direction, bool isRunning, PlayerLocomotion playerLocomotion)
        {
            playerLocomotion.Direction = direction;

            if (playerLocomotion.Direction.magnitude <= 0.125)
            {
                playerLocomotion.movementState = MovementState.Idle;
                return;
            }

            playerLocomotion.movementState = (isRunning && playerLocomotion.physicSim.isGrounded) ? MovementState.Run : MovementState.Walk;
        }

        public static void ProcessJumpAction(PlayerLocomotion playerLocomotion)
        {
            var canJump = playerLocomotion.physicSim.isGrounded;

            if (canJump)
            {
                playerLocomotion.isJumping = true;
                playerLocomotion.rb.AddForce(new Vector2(0f, playerLocomotion.Jumpforce));
            }
        }

        public static void ProcessClimbAction(PlayerLocomotion playerLocomotion)
        {
            playerLocomotion.movementState = MovementState.Climb;
        }

        public static void ProcessClimbMovement(PlayerLocomotion playerLocomotion, Vector2 direction)
        {
            playerLocomotion.Direction = direction;
        }

        public static void ProcessStopClimbAction(PlayerLocomotion playerLocomotion)
        {
            playerLocomotion.movementState = MovementState.Idle;
            playerLocomotion.playerClimb.StopClimb();
        }

        public static void ProcessHangingMovement(PlayerLocomotion playerLocomotion, Vector2 direction, float dt)
        {
            playerLocomotion.movementState = MovementState.Hanging;
            playerLocomotion.playerPiton.deltaY += -direction.y * dt * playerLocomotion.playerPiton.Speed;
            playerLocomotion.Direction = direction;
        }
    }
}