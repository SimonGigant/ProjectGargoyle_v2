namespace PGS.GP.Player
{
    public class MotionComponent : Game.Component
    {
        public PGS.Motion motion;

        public override void UpdateComponent(float dt)
        {
            this.motion = PGS.Motion.None;
        }

        public override void LateUpdateComponent(float dt)
        {
            this.motion.ApplyWorldSpace(this.transform);
        }

        public void Add(Motion motion)
        {
            this.motion.Add(motion);
        }

        public void Add(UnityEngine.Vector3 translation)
        {
            var motion = default(PGS.Motion);
            motion.Translation = translation;
            this.motion.Add(motion);
        }
    }
}