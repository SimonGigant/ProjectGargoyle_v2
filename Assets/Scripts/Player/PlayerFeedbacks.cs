using System;
using System.Collections;
using System.Collections.Generic;
using _3C;
using UnityEngine;

public class PlayerFeedbacks : MonoBehaviour
{
    [SerializeField] private GameObject JumpVFX;
    [SerializeField] private GameObject RunVFX;

    private void Start()
    {
        PlayerCharacter player = GetComponent<PlayerCharacter>();
        if (player != null)
        {
            player.onJumpEvent.AddListener(OnJump);
        }
    }

    //Received when animation plays a step
    public void AnimStep()
    {
        OnRun();
    }

    public void OnJump()
    {
        var transform1 = transform;
        Instantiate(JumpVFX, transform1.position, transform1.rotation);
    }

    public void OnRun()
    {
        var transform1 = transform;
        Instantiate(RunVFX, transform1.position, transform1.rotation);
    }
}
