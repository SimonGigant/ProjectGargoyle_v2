using System;
using UnityEngine;

namespace _3C
{
    public class WallCheck : MonoBehaviour
    {
        private PlayerCharacter _player;
        private Collider2D col;
        private void Start()
        {
            _player = GetComponentInParent<PlayerCharacter>();
            col = GetComponent<Collider2D>();
        }

        private void OnTriggerStay2D(Collider2D other)
        {
            if(col.IsTouchingLayers(LayerMask.GetMask("Climbable")))
                _player.OnWallCollision();
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if(!col.IsTouchingLayers(LayerMask.GetMask("Climbable")))
                _player.OnWallNotFound();
        }
    }
}
