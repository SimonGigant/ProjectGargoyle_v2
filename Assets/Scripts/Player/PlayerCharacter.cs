using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using Quaternion = UnityEngine.Quaternion;
using Sequence = DG.Tweening.Sequence;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

enum AttachmentState { Unattached, Attached }
public enum MovementState { Unknown, Idle, Walk, Run, Climb, Hanging }
public enum UsingTool { None, Piton, Rope, Magnet, Thrower }

namespace _3C
{
    public class PlayerCharacter : MonoBehaviour
    {
        //Cached
        [HideInInspector] public new Rigidbody2D rigidbody2D;
        private SpriteRenderer _renderer;
        private AimUI _aim;
        private Vector3 _velocity = Vector3.zero;
        private Rope _rope;
        private PlayerAnimations _anim;
        private float _gravityValue;
        private bool _runButtonPressed;
        private bool _climbButtonPressed;
        private CinemachineImpulseSource _cinemachineImpulseSource;

        //Movement parameters (to centralize in a ScriptableObject)
        private float _movementSmoothing = .05f;
        [SerializeField] private float movementSpeed;
        [SerializeField] private float runSpeed;
        [SerializeField] private float climbSpeed;
        [SerializeField] private LayerMask whatIsGround;
        [SerializeField] private Transform groundCheck;
        [SerializeField] private Collider2D platformClimbCheck;
        [SerializeField] private Collider2D wallCheck;
        [SerializeField] private float coyoteTime = 0.2f;
        [SerializeField] private float jumpForce = 9000.0f;
        [SerializeField] private float maxSurvivableHeight;
        public UnityEvent onLandEvent;
        public UnityEvent onJumpEvent;

        //States
        [SerializeField] private AttachmentState attachmentState;
        [SerializeField] private MovementState movementState;
        [SerializeField] private UsingTool toolInHand;

        [SerializeField] private TMP_Text stateText;

        //Piton Thrower
        [SerializeField] private GameObject prefabPitonThrown;
        private const float PitonThrowCooldown = 1.0f;

        //Piton
        public int maxPitonCount = 3;
        [HideInInspector] public int currentPitonCount;
        [SerializeField] private GameObject prefabPiton;
        /**Pitons that have been thrown/placed in the world*/
        private List<Piton> _thrownPitons = new List<Piton>();
        /**Pitons the player is currently attached to*/
        public List<Piton> attachedToPitons;
        /**Used to create a middle ground between the piton and the player to work when the rope collides with a wall*/
        public Vector2 fakeAttachmentPoint;
        public GameObject pitonInHandSprite;
        private float _pitonDistanceToAttach = 2.0f;
        private float _pitonDistanceToDetach = 1.0f;
        private float _pitonCurrentCooldown = 0.0f;
        public bool attachedToPitonInHand = false;
        private Piton _nearestPiton;
        private const float PitonPutCooldown = 0.3f;

        //Climbing
        public Transform center;
        private Vector2 _climbingDirection = Vector2.right;
        private float _offsetFromWall = 0.6f;
        [SerializeField] public float maxStamina = 9.0f;
        /** 0 is vertical, 1 is horizontal */
        [SerializeField] private AnimationCurve staminaLossByAngle;
        private float _currentStamina = 0.0f;
        private bool _canClimb = true;

        [HideInInspector] public UnityEvent<float> onStaminaChanged;

        //Temporary values
        private float _currentSpeed;
        private bool _facingRight = false;
        private bool _grounded = false;
        private bool _jumped = false;
        private Vector2 _currentMovement;
        private Vector2 _currentStick;
        private float _currentFallDistance = 0.0f; //Only for caching some things
        private float _currentFallingHeight = 0.0f; //Used to know real falling distance (to rework 'cause that's dirty)
        private float _currentJumpTimer = 0.0f;
        private float _currentCoyoteTime = 0.0f;
        private const float _groundedRadius = .2f;
        private int _isStretching = 0;
        private bool _cachedJump = false;
        private bool _cooldownUpJumpButton = true;
        private bool _cachedPitonFixedWhenPressing = false;

        private void Awake()
        {
            currentPitonCount = maxPitonCount;
            rigidbody2D = GetComponent<Rigidbody2D>();
            _renderer = GetComponentInChildren<SpriteRenderer>();
            _aim = GetComponentInChildren<AimUI>();
            _aim.SetActive(false);
            _currentSpeed = movementSpeed;
            _rope = GetComponentInChildren<Rope>();
            _anim = GetComponent<PlayerAnimations>();
            _cinemachineImpulseSource = GetComponent<CinemachineImpulseSource>();

            _gravityValue = rigidbody2D.gravityScale;

            attachmentState = AttachmentState.Unattached;
            movementState = MovementState.Walk;
            toolInHand = UsingTool.None;

            _currentStamina = maxStamina;
        }


        private void Start()
        {
            GameManager.Instance.Player = this;
        }

        private void Update()
        {
            UpdatePitonHighlight();
        }

        /**
         * Manage the walk/run movement every tick
         */
        private void Move(float value)
        {
            if (movementState == MovementState.Walk && _grounded && _runButtonPressed)
                ChangeMovementState(MovementState.Run);
            else if (movementState == MovementState.Run && !_runButtonPressed)
                ChangeMovementState(MovementState.Walk);

            _anim.ParamSpeed = Mathf.Abs(value);

            // Move the character by finding the target velocity
            float accVelocity = 0.0f;
            if (movementState == MovementState.Walk)
                _currentSpeed = value * movementSpeed;
            else if (movementState == MovementState.Run)
                _currentSpeed = Mathf.SmoothDamp(_currentSpeed, value * runSpeed, ref accVelocity, _movementSmoothing * 2.0f);


            var velocity = rigidbody2D.velocity;
            Vector3 targetVelocity = new Vector2(_currentSpeed, velocity.y);

            //attachement:
            /*Vector3 previousPos = transform.position;*/
            if (attachmentState == AttachmentState.Attached)
            {
                if (_rope.GetRopeLengthLastSection() > _rope.maxRopeLength)
                {
                    DetachFromEveryPiton();
                }
                //transform.position = _rope.ComputeMaxPositionThroughRope(transform.position);
            }
            /*
            if (previousPos != transform.position)
            {
                targetVelocity = new Vector2(m_CurrentSpeed, 0.0f);
            }*/


            // And then smoothing it out and applying it to the character
            rigidbody2D.velocity = Vector3.SmoothDamp(velocity, targetVelocity, ref _velocity, _movementSmoothing);

            if (_grounded)
            {
                if (value > 0 && !_facingRight)
                {
                    Flip();
                }
                else if (value < 0 && _facingRight)
                {
                    Flip();
                }
            }
        }

        /**
         * Manage the hanging movement every tick
         */
        private void HangFromRope()
        {
            if (attachedToPitons.Count < 1)
            {
                ChangeMovementState(MovementState.Walk);
                return;
            }
            float value = _currentMovement.y;
            Vector2 pitonPosition = attachedToPitons[attachedToPitons.Count - 1].transform.position;
            //Vector2 axis = Vector2.up;
            Vector2 axis = (pitonPosition - (Vector2)(center.position)).normalized;
            if (value < 0.0f)
                axis.x = -axis.x;

            if (pitonPosition.y <= center.position.y && value > 0.0f)
            {
                //Cannot climb upward the last attached piton
                Vector3 targetVelocity = Vector3.zero;
                rigidbody2D.velocity =
                    Vector3.SmoothDamp(rigidbody2D.velocity, targetVelocity, ref _velocity, _movementSmoothing);
            }
            else if (_rope.GetRopeLengthLastSection() >= _rope.maxRopeLength && value < 0.0f)
            {
                rigidbody2D.velocity = Vector3.zero;
            }
            else
            {
                Vector3 targetVelocity = axis.normalized * (value * climbSpeed);
                rigidbody2D.velocity =
                    Vector3.SmoothDamp(rigidbody2D.velocity, targetVelocity, ref _velocity, _movementSmoothing);
            }
            if (_currentMovement.x > 0 && !_facingRight)
            {
                Flip();
            }
            else if (_currentMovement.x < 0 && _facingRight)
            {
                Flip();
            }
        }

        /**
         * Manage the climbing movement every tick
         */
        private void Climb()
        {
            Vector2 axis = Vector2.Perpendicular(_climbingDirection);
            if (axis.y < 0.0f)
                axis = -axis;
            axis.Normalize();

            float value = Vector2.Dot(_currentMovement, axis);

            if (ComputeClimbDirection((Vector2)center.position + (axis.normalized * (value * climbSpeed * Time.deltaTime)), out Vector2 bestPosition)
            || ((axis * Mathf.Sign(value)).y > 0.0f && wallCheck.IsTouchingLayers(LayerMask.GetMask("Climbable"))))
            {
                if (_climbingDirection.y <= -0.3f && _currentMovement.y < 0.0f)
                {
                    rigidbody2D.velocity = Vector2.zero;
                    return;
                }


                Vector3 targetVelocity = axis.normalized * (value * climbSpeed);
                Vector2 newPos = bestPosition +
                                 (Vector2)(targetVelocity * Time.deltaTime);
                rigidbody2D.isKinematic = true;
                transform.position = Vector3.Lerp(transform.position, bestPosition, Time.deltaTime);
                rigidbody2D.isKinematic = false;
                rigidbody2D.velocity = Vector3.SmoothDamp(rigidbody2D.velocity, targetVelocity, ref _velocity, _movementSmoothing);

                if (ShouldLoseStamina())
                {
                    float xValue = Mathf.Abs(axis.x);
                    AddStamina(-staminaLossByAngle.Evaluate(xValue) * Time.fixedDeltaTime);
                }
                else
                {
                    AddStamina(Time.fixedDeltaTime * 1.0f);
                }
            }
            else
            {
                ChangeMovementState(MovementState.Walk);
            }
        }

        /**
         *
         */
        private bool ShouldLoseStamina()
        {
            if (attachedToPitons.Count > 0)
            {
                foreach (Piton piton in attachedToPitons)
                {
                    if (center.position.y <= piton.transform.position.y)
                        return false;
                }
                float distance = Vector2.Distance(attachedToPitons[attachedToPitons.Count - 1].transform.position, center.transform.position);
                if (distance <= 1.0f)
                    return false;
            }
            return true;
        }

        //Add or remove this amount of stamina
        private void AddStamina(float value)
        {
            _currentStamina += value;
            _currentStamina = Mathf.Clamp(_currentStamina, 0.0f, maxStamina);
            onStaminaChanged.Invoke(_currentStamina);
            if (_currentStamina <= 0.0f)
            {
                StaminaDepleted();
            }
        }

        /**
         * When there is no stamina left
         */
        private void StaminaDepleted()
        {
            ChangeMovementState(attachedToPitons.Count > 0 ? MovementState.Hanging : MovementState.Walk);
        }

        /*
         * Compute and set the climbing direction (which is the direction from the character to the climbed wall)
         * also return if there is a wall and what is the position the character is supposed to be at, with the right offset
         */
        private bool ComputeClimbDirection(Vector2 newPosition, out Vector2 bestPosition)
        {
            Vector3 position = newPosition;
            RaycastHit2D hit =
                Physics2D.Raycast(position, _climbingDirection, 2.0f, LayerMask.GetMask("Climbable"));
            position.z = 0;
            if (hit.normal != Vector2.zero)
                _climbingDirection = -hit.normal;
            else
            {
                bestPosition = center.position;
                return false;
            }

            bestPosition = hit.point + (hit.normal * _offsetFromWall) + (Vector2)(transform.position) - newPosition;
            return true;
        }

        private void ChangeMovementState(MovementState newState)
        {
            if (newState == movementState)
                return;

            stateText.SetText(newState.ToString());

            switch (newState)
            {
                case MovementState.Run:
                case MovementState.Walk:
                    rigidbody2D.gravityScale = _gravityValue;
                    _climbingDirection = _facingRight ? Vector2.right : Vector2.left;
                    _anim.SetAnimation(PlayerAnimState.Run);
                    break;
                case MovementState.Climb:
                    rigidbody2D.gravityScale = 0.0f;
                    ComputeClimbDirection(center.position, out Vector2 bestPosition);
                    //rigidbody2D.DOMove(bestPosition, 0.2f);
                    _anim.SetAnimation(PlayerAnimState.Climb);
                    break;
                case MovementState.Hanging:
                    rigidbody2D.gravityScale = 0.0f;
                    float targetXPos = ((Vector2)attachedToPitons[attachedToPitons.Count - 1].transform.position - Vector2.up * 3.0f).x;
                    float targetYPos = Mathf.Min(((Vector2)attachedToPitons[attachedToPitons.Count - 1].transform.position -
                                        Vector2.up * 3.0f).y, center.position.y);
                    rigidbody2D.DOMove(new Vector2(targetXPos, targetYPos), 0.6f);
                    rigidbody2D.velocity = Vector2.zero;
                    StartCoroutine(OnHanging());
                    _anim.SetAnimation(PlayerAnimState.Hanging);
                    break;
            }
            movementState = newState;
        }

        private IEnumerator OnHanging()
        {
            _canClimb = false;
            yield return new WaitForSeconds(0.6f);
            _canClimb = true;
        }

        /**
         * Starts a jump. The results depends on current state
         */
        private void Jump()
        {
            StopCoroutine(ResetCacheJump());
            _cachedJump = false;
            _jumped = true;
            _currentCoyoteTime = coyoteTime + 0.1f;
            _currentJumpTimer = 0.0f;
            switch (movementState)
            {
                case MovementState.Run:
                case MovementState.Walk:
                    rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0.0f);
                    rigidbody2D.AddForce(new Vector2(0f, jumpForce));
                    break;
                case MovementState.Climb:
                    ChangeMovementState(MovementState.Walk);
                    rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
                    float dir = _facingRight ? -1.0f : 1.0f;
                    rigidbody2D.AddForce(new Vector2(2.0f * jumpForce * dir, 0.0f));
                    Flip();
                    break;
                case MovementState.Hanging:
                    return;
            }
            onJumpEvent.Invoke();
            StartCoroutine(StretchAndSquash(false));
        }

        /**
         * Flip the sprite direction
         */
        private void Flip()
        {
            // Switch the way the player is labelled as facing.
            _facingRight = !_facingRight;

            _climbingDirection = _facingRight ? Vector2.right : Vector2.left;

            // Multiply the player's x local scale by -1.
            Transform trans = _renderer.transform;
            Vector3 theScale = trans.localScale;
            theScale.x *= -1;
            trans.localScale = theScale;
        }

        private Sequence _stretchSequence;

        /**
         * Starts stretching and squashing the sprite if landing/jumping
         */
        private IEnumerator StretchAndSquash(bool landing = false)
        {
            if (_isStretching != (landing ? -1 : 1))
            {
                _isStretching = landing ? -1 : 1;
                _stretchSequence.Complete();
                _stretchSequence = DOTween.Sequence().Pause();
                if (landing)
                {
                    Vector3 fromScale = Vector3.one;
                    Vector3 toScale = new Vector3(1.2f, 0.8f, 1.0f);
                    _stretchSequence.Append(transform.DOScale(toScale, 0.3f).SetEase(Ease.OutQuad).From())
                        .Append(transform.DOScale(fromScale, 0.05f).SetEase(Ease.InOutQuad));
                }
                else
                {
                    Vector3 fromScale = Vector3.one;
                    Vector3 toScale = new Vector3(0.75f, 1.35f, 1.0f);
                    _stretchSequence.Append(transform.DOScale(toScale, 0.3f).SetEase(Ease.InOutCubic).From())
                        .Append(transform.DOScale(fromScale, 0.05f).SetEase(Ease.InOutQuad));
                }
                _stretchSequence.Play();
                yield return new WaitForSeconds(_stretchSequence.Duration());
                _isStretching = 0;
            }
        }


        //Where should a piton spawn when facing the character?
        private Vector2 ComputePitonPosition()
        {
            Vector2 direction = _facingRight ? Vector2.right : Vector2.left;
            RaycastHit2D rc = Physics2D.CircleCast(center.position, 0.6f, direction, 2.0f, whatIsGround);
            if (rc.collider == null)
                return transform.position;
            return rc.point;
        }

        //Where should a piton spawn when underneath the character?
        private Vector2 ComputePitonPositionDown()
        {
            RaycastHit2D rc = Physics2D.CircleCast(center.position, 0.6f, Vector2.down, 2.0f, whatIsGround);
            if (rc.collider == null)
                return transform.position;
            return rc.point;
        }


        /**
         * Fix a piton to the ground underneath the character
         */
        private void FixPitonToGround()
        {
            if (_grounded)
            {
                Transform trans = transform;
                GameObject attachedGameObject = GameObject.Instantiate(prefabPiton, ComputePitonPositionDown(), trans.rotation);
                Gamefeel.Instance.InitScreenshake(0.15f, 0.3f);
                attachedGameObject.GetComponent<Piton>().player = this;

                SpriteRenderer pitonInHandRenderer = pitonInHandSprite.GetComponent<SpriteRenderer>();

                pitonInHandRenderer.enabled = false;

                if (attachedToPitonInHand && toolInHand == UsingTool.Piton)
                {
                    AttachToPiton(attachedGameObject.GetComponent<Piton>());
                }
                if (toolInHand == UsingTool.Piton)
                {
                    toolInHand = UsingTool.None;
                }
            }
        }

        /**
         * Fix a piton to the wall where the character face
         */
        private void FixPitonToWall()
        {
            Transform trans = transform;
            GameObject attachedGameObject = GameObject.Instantiate(prefabPiton, ComputePitonPosition(), trans.rotation);
            Gamefeel.Instance.InitScreenshake(0.15f, 0.3f);
            attachedGameObject.GetComponent<Piton>().player = this;


            SpriteRenderer pitonInHandRenderer = pitonInHandSprite.GetComponent<SpriteRenderer>();

            pitonInHandRenderer.enabled = false;

            if (attachedToPitonInHand && toolInHand == UsingTool.Piton)
            {
                AttachToPiton(attachedGameObject.GetComponent<Piton>());
            }
            if (toolInHand == UsingTool.Piton)
            {
                toolInHand = UsingTool.None;
            }
        }

        private void FixPitonAtPosition(Vector2 position)
        {
            Transform trans = transform;
            GameObject attachedGameObject = GameObject.Instantiate(prefabPiton, position, trans.rotation);
            attachedGameObject.GetComponent<Piton>().player = this;
            AttachToPiton(attachedGameObject.GetComponent<Piton>());
        }

        private void RemoveEveryThrownPiton()
        {
            foreach (Piton pit in _thrownPitons)
            {
                DetachFromPiton(pit);
                pit.RemovePiton();
            }

            _thrownPitons.Clear();
            attachedToPitons.Clear();
            currentPitonCount = maxPitonCount;
        }



        //When an outside elements fix a piton (for example when shooting with the piton thrower)
        public void OnPitonFixed(Piton piton)
        {
            _thrownPitons.Add(piton);
        }

        //When an outside elements remove a piton
        public void OnPitonUnfixed(Piton piton)
        {
            _thrownPitons.Remove(piton);
        }

        /**
         * Update the highlight state of every piton
         */
        private void UpdatePitonHighlight()
        {
            foreach (Piton piton in _thrownPitons)
            {
                piton.StopHighlight();
            }

            bool res;
            _nearestPiton = GetNearestPiton(out res, false);
            if (res)
                _nearestPiton.Highlight();
            else
                _nearestPiton = null;
        }

        /**
        * Return the piton closest to the character (whether it is attached or not)
*/
        private Piton GetNearestPiton(out bool isNearEnough, bool attach = true)
        {
            Piton best = null;
            float sqrDist = 0.0f;

            foreach (Piton piton in _thrownPitons)
            {
                Vector3 position1 = center.position;
                Vector3 position2 = piton.transform.position;
                float thisSqrDist =
                    Vector2.SqrMagnitude(new Vector2(position2.x, position2.y) -
                                         new Vector2(position1.x, position1.y));
                if (best == null || sqrDist > thisSqrDist)
                {
                    sqrDist = thisSqrDist;
                    best = piton;
                }
            }
            if (attach)
                isNearEnough = best != null && sqrDist < _pitonDistanceToAttach * _pitonDistanceToAttach;
            else
                isNearEnough = best != null && sqrDist < _pitonDistanceToDetach * _pitonDistanceToDetach;
            return best;
        }

        /**
        * Return the attached piton closest to the character
*/
        private Piton GetNearestAttachedPiton()
        {
            Piton best = null;
            float sqrDist = 0.0f;

            foreach (Piton piton in attachedToPitons)
            {
                Vector3 position1 = transform.position;
                Vector3 position2 = piton.transform.position;
                float thisSqrDist =
                    Vector2.SqrMagnitude(new Vector2(position2.x, position2.y) -
                                         new Vector2(position1.x, position1.y));
                if (best == null || sqrDist > thisSqrDist)
                {
                    sqrDist = thisSqrDist;
                    best = piton;
                }
            }

            return best;
        }
        /**
        * Attach the character to the given piton
*/
        private void AttachToPiton(Piton piton)
        {
            if (piton == null || attachedToPitons.Contains(piton))
                return;

            attachedToPitons.Add(piton);
            attachmentState = AttachmentState.Attached;
        }

        /**
        * Detach the character from given piton
*/
        private void DetachFromPiton(Piton piton)
        {
            if (piton == null || !attachedToPitons.Contains(piton))
                return;

            attachedToPitons.Remove(piton);
            if (attachedToPitons.Count == 0)
            {
                attachmentState = AttachmentState.Unattached;
                if (movementState == MovementState.Hanging)
                    ChangeMovementState(MovementState.Walk);
            }
        }

        //Detach the character entirely
        private void DetachFromEveryPiton()
        {
            attachedToPitons.Clear();
            if (attachedToPitons.Count == 0)
                attachmentState = AttachmentState.Unattached;
        }

        //The directional vector from the attached piton to the rope socket on the character
        private Vector2 GetDirectionFromRopeBase()
        {
            if (attachedToPitons.Count == 0)
                return Vector2.zero;
            Vector2 position1 = _rope.ropeStart.position;
            Vector2 position2 = attachedToPitons[attachedToPitons.Count - 1].transform.position;

            Vector2 res = (position1 - position2).normalized;
            return res;
        }

        private Vector2 GetAimDirection()
        {
            return _currentStick.normalized;
        }

        //When the character lands on floor
        private void OnLanding()
        {
            if (movementState == MovementState.Walk)
            {
                if (_currentFallingHeight >= maxSurvivableHeight)
                {
                    _cinemachineImpulseSource.GenerateImpulse(new Vector3(2, 2, 0));
                    Debug.LogWarning("YOU DIED");
                }
            }
            if (movementState == MovementState.Hanging)
            {
                ChangeMovementState(MovementState.Walk);
            }
            _currentFallingHeight = 0.0f;
            onLandEvent.Invoke();
        }

        private void FixedUpdate()
        {
            _pitonCurrentCooldown += Time.fixedDeltaTime;

            if (_currentCoyoteTime < coyoteTime)
                _currentCoyoteTime += Time.fixedDeltaTime;

            _currentJumpTimer += Time.fixedDeltaTime;

            //Check if grounded
            bool wasGrounded = _grounded;
            _grounded = false;
            if (rigidbody2D.velocity.y <= 3.0f)
            {
                Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, _groundedRadius, whatIsGround);
                foreach (Collider2D t in colliders)
                {
                    if (t.gameObject != gameObject)
                    {
                        _grounded = true;
                        _jumped = false;
                        if (!wasGrounded)
                        {
                            if (_currentFallDistance > 0.5f || _jumped)
                            {
                                if (movementState == MovementState.Walk || movementState == MovementState.Run)
                                    StartCoroutine(StretchAndSquash(true));
                            }
                            OnLanding();
                        }
                    }
                }
            }

            if (_grounded)
                AddStamina(Time.fixedDeltaTime * 5.0f);
            _anim.ParamGrounded = _grounded;


            MovementsFixedUpdate(wasGrounded);
        }

        /**
        * Compute everything related to movements each fixedTime tick
*/
        private void MovementsFixedUpdate(bool wasGrounded)
        {
            //Manage to jump if the character just landed and had a cached jump input
            if (_cachedJump && _grounded && !wasGrounded && !_jumped)
            {
                StartCoroutine(ResetJumpCooldown());
                Jump();
            }

            //Reset fall data if just started to fall
            if (wasGrounded && !_grounded && !_jumped && rigidbody2D.velocity.y <= 0.0f)
            {
                _currentCoyoteTime = 0.0f;
                _currentFallDistance = 0.0f;
                _currentJumpTimer = 0.0f;
            }

            //If going downward, update fall data
            if (rigidbody2D.velocity.y < 0.0f)
            {
                _currentFallDistance += -rigidbody2D.velocity.y * Time.fixedDeltaTime;
                if (movementState == MovementState.Walk || movementState == MovementState.Run)
                    _currentFallingHeight += -rigidbody2D.velocity.y * Time.fixedDeltaTime;
                //Debug.Log(_currentFallDistance);
                if (_currentFallDistance > 1.0f)
                    CheckBrokenRope();
            }

            //Calls right movement method depending on movement state
            switch (movementState)
            {
                case MovementState.Climb:
                    Climb();
                    break;
                case MovementState.Hanging:
                    HangFromRope();
                    break;
                case MovementState.Walk:
                case MovementState.Run:
                    Move(_currentMovement.x);
                    break;
            }

            if (_grounded)
            {
                _currentFallingHeight = 0.0f;
            }
        }

        /**
        * Check if the last segment of the rope cannot be continuous
        *
*/
        private void CheckBrokenRope()
        {
            if (attachedToPitons.Count == 0)
                return;
            Vector2 from = center.position;
            Vector2 to = attachedToPitons[attachedToPitons.Count - 1].transform.position;
            float distSqr = Vector2.SqrMagnitude(to - from);
            const float maxDist = 3.0f;
            if ((from.x - to.x) > 3.0f || (from.y > to.y) || distSqr > maxDist * maxDist)
                return;
            RaycastHit2D rc = Physics2D.Raycast(from, (to - from).normalized, (to - from).magnitude, whatIsGround);
            if (rc.collider != null)
            {
                fakeAttachmentPoint = rc.point;
                //For the moment, place a piton there, should do it differently
                FixPitonAtPosition(rc.point);
                ChangeMovementState(MovementState.Hanging);
            }
        }

        /**
        * Is there a wall near in the given direction? (right or left only)
*/
        private bool CastDirection(bool right)
        {
            Vector2 direction = right ? Vector2.right : Vector2.left;
            RaycastHit2D rc = Physics2D.Raycast(center.position, direction, _offsetFromWall, whatIsGround);
            return rc.collider != null;
        }

        //*********************************************************************************************************************//
        //Input Management:
        //*********************************************************************************************************************//

        /**
        * When the movement stick/keys change
*/
        public void OnMovementInput(InputAction.CallbackContext ctx)
        {
            Vector2 value = ctx.ReadValue<Vector2>();
            _currentMovement = new Vector2(Mathf.Clamp(value.x, -1.0f, 1.0f), Mathf.Clamp(value.y, -1.0f, 1.0f));
        }

        /**
        * When the right stick changes
        * (to aim with the piton thrower and to pull the rope only atm)
*/
        public void OnAimInput(InputAction.CallbackContext ctx)
        {
            _currentStick = ctx.ReadValue<Vector2>();
            _aim.SetActive(_currentStick.magnitude > 0.3f);
            float angle = Mathf.Atan2(_currentStick.y, _currentStick.x) * Mathf.Rad2Deg;
            _aim.transform.rotation = Quaternion.Euler(0.0f, 0.0f, angle);


            if (!_ropePullStarted && IsRopePullValid())
                StartCoroutine(StartPullingRope());
        }

        private bool _triggeredClimb = false;

        /**
        * When the run button is pressed
        * (for the moment used for climbing too)
*/
        public void OnRunInput(InputAction.CallbackContext ctx)
        {
            _runButtonPressed = ctx.ReadValueAsButton();

        }

        public void OnClimbInput(InputAction.CallbackContext ctx)
        {
            _climbButtonPressed = ctx.ReadValueAsButton();
            if (!_climbButtonPressed)
            {
                _triggeredClimb = false;
            }
            else if (_climbButtonPressed && movementState == MovementState.Climb && !_triggeredClimb)
            {
                ChangeMovementState(MovementState.Walk);
                _triggeredClimb = true;
            }
        }

        /**
        * When the jump input is pressed
*/
        public void OnJumpInput(InputAction.CallbackContext ctx)
        {
            if (ctx.ReadValueAsButton() && _cooldownUpJumpButton)
            {
                StartCoroutine(ResetJumpCooldown());

                if (!_jumped && !_cachedJump && (movementState == MovementState.Climb || _grounded ||
                                                 _currentCoyoteTime < coyoteTime))
                {
                    Jump();
                }
                else if (!_grounded && !_cachedJump && _currentCoyoteTime >= coyoteTime)
                {
                    StartCoroutine(ResetCacheJump());
                }
            }
        }

        /**
        * When the "hang from rope" input is pressed
*/
        public void OnHangInput(InputAction.CallbackContext ctx)
        {
            if (ctx.ReadValueAsButton() && movementState == MovementState.Climb && attachedToPitons.Count > 0)
            {
                ChangeMovementState(MovementState.Hanging);
            }
            else if (ctx.ReadValueAsButton() && movementState == MovementState.Hanging && _canClimb)
            {
                ChangeMovementState(MovementState.Walk);
            }
        }

        private IEnumerator ResetJumpCooldown()
        {
            _cooldownUpJumpButton = false;
            yield return new WaitForSeconds(0.3f);
            _cooldownUpJumpButton = true;
        }

        private IEnumerator ResetCacheJump()
        {
            _cachedJump = true;
            yield return new WaitForSeconds(0.3f);
            _cachedJump = false;
        }

        /**
        * When the rope input is pressed. The rope input serves multiple purposes
*/
        public void OnRopeInput(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                _ropeInputIsPressed = true;
                if (toolInHand == UsingTool.Piton)
                {
                    attachedToPitonInHand = !attachedToPitonInHand;
                    return;
                }
                else if (_nearestPiton != null)
                {
                    if (attachedToPitons.Contains(_nearestPiton))
                    {
                        DetachFromPiton(_nearestPiton);
                        return;
                    }
                    else
                    {
                        AttachToPiton(_nearestPiton);
                        return;
                    }
                }
                if (!_ropePullStarted && IsRopePullValid())
                    StartCoroutine(StartPullingRope());
            }
            if (ctx.canceled)
            {
                _ropeInputIsPressed = false;
            }
        }

        private bool _ropePullStarted = false;
        private bool _ropeInputIsPressed = false;

        private IEnumerator StartPullingRope()
        {
            _ropePullStarted = true;
            bool canceled = false;
            float count = 0.0f;
            _rope.StartShake();
            for (; ; )
            {
                yield return null;
                if (!IsRopePullValid())
                {
                    canceled = true;
                    break;
                }
                count += Time.deltaTime;
                if (count > 1.0f)
                    break;
            }
            _ropePullStarted = false;
            _rope.StopShake();
            if (!canceled && attachedToPitons.Count > 0)
            {
                //DetachFromPiton(attachedToPitons[0]);
                DetachFromEveryPiton();
                foreach (Piton pit in _thrownPitons)
                {
                    Destroy(pit);
                }
                _thrownPitons.Clear();
                Gamefeel.Instance.InitScreenshake(0.2f, 0.2f);
            }
        }

        // Is the stick aiming in the opposite direction when the character is attached and the rope button is pressed?
        private bool IsRopePullValid()
        {
            if (_ropeInputIsPressed && attachmentState == AttachmentState.Attached && _currentStick.magnitude > 0.5f)
            {
                Vector2 aimDir = GetAimDirection();
                Vector2 ropeDir = GetDirectionFromRopeBase();
                float projectionMagnitude = Vector2.Dot(aimDir, ropeDir);
                return projectionMagnitude > 0.7f;
            }

            return false;
        }

        /**
        * When the piton input changes. Used to put pitons on the rock
*/
        public void OnPitonInput(InputAction.CallbackContext ctx)
        {
            if (currentPitonCount <= 0)
                return;
            if (_pitonCurrentCooldown <= PitonPutCooldown)
                return;

            SpriteRenderer pitonInHandRenderer = pitonInHandSprite.GetComponent<SpriteRenderer>();

            if (ctx.ReadValueAsButton() && !_cachedPitonFixedWhenPressing && (toolInHand == UsingTool.None || toolInHand == UsingTool.Piton))
            {
                if (movementState == MovementState.Climb)
                {
                    _pitonCurrentCooldown = 0.0f;
                    //to make the piton directly attached:
                    toolInHand = UsingTool.Piton;
                    attachedToPitonInHand = true;
                    --currentPitonCount;
                    FixPitonToWall();
                }
                else if (_grounded && _currentMovement.y <= -0.7f)
                {
                    _pitonCurrentCooldown = 0.0f;
                    toolInHand = UsingTool.Piton;
                    attachedToPitonInHand = true;
                    --currentPitonCount;
                    FixPitonToGround();
                }
                else
                {
                    toolInHand = UsingTool.Piton;
                    pitonInHandRenderer.enabled = true;
                }
            }
            else
            {
                if (toolInHand == UsingTool.Piton)
                {
                    toolInHand = UsingTool.None;
                }
                pitonInHandRenderer.enabled = false;

                if (!ctx.ReadValueAsButton())
                    _cachedPitonFixedWhenPressing = false;
            }
        }

        /**
        * Called when there is a collision with a wall in front of the character
*/
        public void OnWallCollision()
        {
            if (_climbButtonPressed && _currentStamina > 0.0f && _canClimb && !_triggeredClimb)
            {
                Vector3 position = center.position;
                RaycastHit2D hit =
                    Physics2D.Raycast(position, _climbingDirection, 2.0f, LayerMask.GetMask("Climbable"));
                if (hit.normal.magnitude >= 0.9f && hit.normal.y <= 0.01f)
                {
                    _triggeredClimb = true;
                    ChangeMovementState(MovementState.Climb);
                }
            }
            if (toolInHand == UsingTool.Piton)
            {
                //to make the piton directly attached:
                attachedToPitonInHand = true;

                --currentPitonCount;
                FixPitonToWall();
                _cachedPitonFixedWhenPressing = true;
                toolInHand = UsingTool.None;
            }
        }

        /**
        * Teleport the character on top of the edge if it manages to be close and high enough
        * TODO: refine for edges that are really low
*/
        private void LedgeTeleport()
        {
            Vector2 offset = wallCheck.offset;
            offset.y = 0.1f;
            if (_facingRight)
                offset.x = -offset.x;
            rigidbody2D.DOMove(((Vector2)(wallCheck.transform.position) + offset), 0.2f);
            rigidbody2D.isKinematic = true;
            StartCoroutine(RemoveKinematic(0.2f));
            rigidbody2D.velocity = Vector2.zero;
            if (movementState == MovementState.Climb)
                ChangeMovementState(MovementState.Walk);
        }

        private IEnumerator RemoveKinematic(float duration)
        {
            yield return new WaitForSeconds(duration);
            rigidbody2D.isKinematic = false;
        }

        public void OnWallNotFound()
        {
            if (platformClimbCheck.IsTouchingLayers(LayerMask.GetMask("Climbable")) && !_grounded && (_currentJumpTimer > 0.05f || movementState == MovementState.Climb))
            {
                LedgeTeleport();
            }
        }

        public void OnMagnetInput(InputAction.CallbackContext ctx)
        {
            RemoveEveryThrownPiton();
        }

        //Cheat
        public void OnRespawn(InputAction.CallbackContext ctx)
        {
            GameManager.Instance.RespawnScene();
        }










        //!\\
        //DEPRECATED AREA, be careful:

        private bool _ropeLengthChangeCooldown = true;

        private IEnumerator ResetChangeLengthCooldown()
        {
            _ropeLengthChangeCooldown = false;
            yield return new WaitForSeconds(0.5f);
            _ropeLengthChangeCooldown = true;
        }

        //DEPRECATED?
        private void RopeChangeLength()
        {
            if (!_ropeLengthChangeCooldown || _currentMovement.magnitude < 0.6f)
                return;
            Vector2 ropeDirection = GetDirectionFromRopeBase();
            Vector2 stickDirection = _currentMovement.normalized;
            float projectionMagnitude = Vector2.Dot(stickDirection, ropeDirection);
            bool isLengthening = projectionMagnitude > 0.7f;
            bool isShortening = projectionMagnitude < -0.7f;
            if (isLengthening)
                Debug.Log("lengthen");
            if (isShortening)
                Debug.Log("shorten");

            if (isLengthening)
            {
                //_rope.LengthenRope();
                StartCoroutine(ResetChangeLengthCooldown());
            }
            else if (isShortening)
            {
                //_rope.ShortenRope();
                StartCoroutine(ResetChangeLengthCooldown());
            }
        }


        public void OnThrowPitonInput(InputAction.CallbackContext ctx)
        {
            if (_pitonCurrentCooldown <= PitonThrowCooldown)
                return;
            if (ctx.performed && _currentStick.magnitude > 0.3f)
            {
                Gamefeel.Instance.InitScreenshake(0.1f, 0.1f);
                float angle = Mathf.Atan2(_currentStick.y, _currentStick.x) * Mathf.Rad2Deg - 90.0f;
                GameObject pitonThrown = GameObject.Instantiate(prefabPitonThrown, center.position, Quaternion.AngleAxis(angle, Vector3.forward));
                pitonThrown.GetComponent<ThrownPiton>().player = this;
                _pitonCurrentCooldown = 0.0f;
            }
        }

        public void OnPlacePitonInput(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {

            }
        }

        public void OnRemovePitonInput(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                if (attachmentState == AttachmentState.Attached)
                {
                    Piton nearestPiton = GetNearestPiton(out bool isNearEnough, false);
                    if (isNearEnough)
                        DetachFromPiton(nearestPiton);
                }
            }
        }

        public void OnAttachInput(InputAction.CallbackContext ctx)
        {
            if (ctx.performed)
            {
                if (true)
                {
                    Piton nearestPiton = GetNearestPiton(out bool isNearEnough);
                    if (isNearEnough)
                        AttachToPiton(nearestPiton);
                }
            }
        }


    }
}
