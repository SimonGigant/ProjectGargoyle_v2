using UnityEngine;

namespace _3C
{
    public class AimUI : MonoBehaviour
    {
        private bool m_IsActive;

        public void SetActive(bool value)
        {
            m_IsActive = value;
            gameObject.SetActive(value);
        }
    }
}
