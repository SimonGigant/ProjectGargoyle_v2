
namespace PGS.GP.Player
{
    public class PlayerActor : UnityEngine.MonoBehaviour, IPlayerActor
    {
        public void Execute(IAction action)
        {
            action.Execute(this.ProvideGameObject());
        }

        private UnityEngine.GameObject ProvideGameObject()
        {
            return this.gameObject;
        }
    }
}
