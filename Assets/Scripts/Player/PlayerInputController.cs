
namespace PGS.GP.Player
{
    using System;
    using PGS.Game;
    using PGS.Input;

    public class PlayerInputController : Component
    {
        public PlayerActor actor;

        public override void OnStart()
        {
            actor = GetComponent<PlayerActor>();
        }

        public override void UpdateComponent(float dt)
        {
            var map = InputMapUtils.GetPlayerMap();

            Move(map);
            Jump(map);
            Climb(map);
            Piton(map);
        }

        private void Piton(MyInputMap map)
        {
            var usePiton = InputUtils.IsActionTriggered(map, MyPlayerInput.Piton);
            if (usePiton)
            {
                actor.Execute(new UsePiton());
            }

        }

        private void Move(MyInputMap map)
        {
            var move = InputUtils.GetValue(map, MyPlayerInput.Move);
            var moveAction = new MoveAction();
            moveAction.Direction = move;
            actor.Execute(moveAction);

        }

        private void Jump(MyInputMap map)
        {
            var shouldJump = InputUtils.IsActionTriggered(map, MyPlayerInput.Jump);

            if (shouldJump)
            {
                actor.Execute(new JumpAction());
            }
        }

        private void Climb(MyInputMap map)
        {
            var isClimbing = InputUtils.IsPressed(map, MyPlayerInput.Climb);
            var onRelease = InputUtils.OnRelease(map, MyPlayerInput.Climb);

            if (isClimbing)
            {
                actor.Execute(new ClimbAction());
            }

            if (onRelease)
            {
                actor.Execute(new StopClimbAction());
            }
        }
    }
}
