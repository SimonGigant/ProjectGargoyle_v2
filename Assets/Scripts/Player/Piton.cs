using System;
using UnityEngine;

namespace _3C
{
    public class Piton : MonoBehaviour
    {
        [HideInInspector] public PlayerCharacter player;
        [SerializeField] private SpriteRenderer m_Highlight;

        private void Start()
        {
            player.OnPitonFixed(this);
        }

        public void Highlight()
        {
            m_Highlight.enabled = true;
        }

        public void StopHighlight()
        {
            m_Highlight.enabled = false;
        }

        public void RemovePiton()
        {
            Destroy(gameObject);
        }
    }
}
