using UnityEngine;

public static class ShapeUtils
{
    public static void DrawPoint(Vector2 p, float size = 1f)
    {
        UnityEngine.Debug.DrawLine(p - Vector2.down * size, p + Vector2.down * size, UnityEngine.Color.green, UnityEngine.Time.deltaTime);
        UnityEngine.Debug.DrawLine(p - Vector2.left * size, p + Vector2.left * size, UnityEngine.Color.green, UnityEngine.Time.deltaTime);
    }
    public static void DrawPoint(Vector2 p, Color c, float size = 1f)
    {
        UnityEngine.Debug.DrawLine(p - Vector2.down * size, p + Vector2.down * size, c, UnityEngine.Time.deltaTime);
        UnityEngine.Debug.DrawLine(p - Vector2.left * size, p + Vector2.left * size, c, UnityEngine.Time.deltaTime);
    }

    public static void DrawPoint(Vector2 p, Color c, float dt, float size = 1f)
    {
        UnityEngine.Debug.DrawLine(p - Vector2.down * size, p + Vector2.down * size, c, dt);
        UnityEngine.Debug.DrawLine(p - Vector2.left * size, p + Vector2.left * size, c, dt);
    }
}