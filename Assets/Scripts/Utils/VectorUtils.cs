using UnityEngine;

public static class Vector3Extenstion
{
    public static Vector2 xz(this Vector3 v)
    {
        return new Vector2(v.x, v.z);
    }

    public static Vector2 xy(this Vector3 v)
    {
        return new Vector2(v.x, v.y);
    }
}

public static class Vector2Extenstion
{
    public static Vector3 x0y(this Vector2 v)
    {
        return new Vector3(v.x, 0, v.y);
    }

    public static Vector3 xy0(this Vector2 v)
    {
        return new Vector3(v.x, v.y, 0);
    }
}