using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    private static InventoryManager _instance;
    
    public static InventoryManager Instance
    {
        get {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<InventoryManager>();
                if (_instance == null)
                {
                    GameObject container = new GameObject("InventoryManager");
                    DontDestroyOnLoad(container);
                    _instance = container.AddComponent<InventoryManager>();
                }
            }
            return _instance;
        }
    }

    private List<string> _inventory;

    public void AddObject(string objectID)
    {
        string noCapObjectID = objectID.ToLowerInvariant();
        _inventory.Add(noCapObjectID);
    }
    
    public void RemoveObject(string objectID)
    {
        string noCapObjectID = objectID.ToLowerInvariant();
        _inventory.Remove(noCapObjectID);
    }

    public bool HasObject(string objectID)
    {
        string noCapObjectID = objectID.ToLowerInvariant();
        return _inventory.Contains(noCapObjectID);
    }
}
