using System;
using System.Collections;
using System.Collections.Generic;
using GigSpeech;
using UnityEditor;
using UnityEngine;

public class InventoryCondition : ProgressionConditionBase
{
    public string objectID;

    public override bool Compute()
    {
        return InventoryManager.Instance.HasObject(objectID);
    }

    public override void DebugPrint()
    {
        base.DebugPrint();
    }
}
