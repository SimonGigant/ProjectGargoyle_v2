namespace PGS.Game
{
    using System.Collections.Generic;
    using UnityEngine;

    public static class GameUtils
    {
        private static MainExecutor executor;

        public static void Register(Component component)
        {
            CreateExcutorIFN();

            if (component.Priority >= 1000)
            {
                executor.PrioritizedComponents.Add(component);
                executor.PrioritizedComponents.Sort(SortComponentsList);
                return;
            }
            executor.Components.Add(component);

            if (component.Priority != 0)
                executor.Components.Sort(SortComponentsList);

        }

        public static void Unregister(Component component)
        {
            executor.Components.Remove(component);
        }

        private static void CreateExcutorIFN()
        {
            if (executor == null)
            {
                var newGameObject = new GameObject("Executor");
                executor = newGameObject.AddComponent<MainExecutor>();
                executor.Components = new List<Component>();
                executor.PrioritizedComponents = new List<Component>();
            }
        }

        private static int SortComponentsList(Component x, Component y)
        {
            return x.Priority > y.Priority ? 1 : -1;
        }
    }
}