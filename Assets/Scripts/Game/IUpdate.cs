public interface IUpdate
{
    public void UpdateComponent(float dt);
}

public interface IRegister
{
    public void RegisterComponent();
}