namespace PGS.Game
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class MainExecutor : MonoBehaviour
    {
        public List<Component> Components;
        public List<Component> PrioritizedComponents;

        private enum Pass { Update, LateUpdate, FixedUpdate }

        public void Update()
        {
            var dt = UnityEngine.Time.deltaTime;
            ExecuteComponents(PrioritizedComponents, dt, Pass.Update);
            ExecuteComponents(Components, dt, Pass.Update);
        }

        public void FixedUpdate()
        {
            var dt = UnityEngine.Time.deltaTime;
            ExecuteComponents(PrioritizedComponents, dt, Pass.FixedUpdate);
            ExecuteComponents(Components, dt, Pass.FixedUpdate);
        }

        public void LateUpdate()
        {
            var dt = UnityEngine.Time.deltaTime;
            ExecuteComponents(PrioritizedComponents, dt, Pass.LateUpdate);
            ExecuteComponents(Components, dt, Pass.LateUpdate);
        }

        private void ExecuteComponents(List<Component> componentsList, float dt, Pass pass)
        {
            switch (pass)
            {
                case Pass.Update:
                    UpdatePass(componentsList, dt);
                    break;
                case Pass.LateUpdate:
                    LateUpdatePass(componentsList, dt);
                    break;
                case Pass.FixedUpdate:
                    FixedUpdatePass(componentsList, dt);
                    break;
            }
        }

        private void UpdatePass(List<Component> componentsList, float dt)
        {
            for (int i = 0; i < componentsList.Count; i++)
            {
                var componentToExecute = componentsList[i];
                componentToExecute.UpdateComponent(dt);
            }
        }

        private void FixedUpdatePass(List<Component> componentsList, float dt)
        {
            for (int i = 0; i < componentsList.Count; i++)
            {
                var componentToExecute = componentsList[i];
                componentToExecute.FixedUpdateComponent(dt);
            }
        }

        private void LateUpdatePass(List<Component> componentsList, float dt)
        {
            for (int i = 0; i < componentsList.Count; i++)
            {
                var componentToExecute = componentsList[i];
                componentToExecute.LateUpdateComponent(dt);
            }
        }
    }
}