namespace PGS.Game
{
    using UnityEngine;

    public abstract class Component : MonoBehaviour, IUpdate, IRegister
    {
        public int Priority;

        public virtual void OnStart() { }
        public virtual void UpdateComponent(float dt) { }
        public virtual void LateUpdateComponent(float dt) { }
        public virtual void FixedUpdateComponent(float dt) { }

        public void Start()
        {
            RegisterComponent();
            OnStart();
        }

        public void OnDestroy()
        {
            UnregisterComponent();
        }

        public void RegisterComponent()
        {
            GameUtils.Register(this);
        }

        public void UnregisterComponent()
        {
            GameUtils.Unregister(this);
        }
    }
}
