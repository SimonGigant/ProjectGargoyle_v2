namespace PGS
{
    using System.Collections.Generic;

    public struct Motion
    {
        public UnityEngine.Vector3 Translation;
        public UnityEngine.Quaternion Rotation;

        public static Motion None
        {
            get
            {
                return new Motion()
                {
                    Translation = UnityEngine.Vector3.zero,
                    Rotation = UnityEngine.Quaternion.identity,
                };
            }
        }

        public void Add(Motion motion)
        {
            this.Translation += motion.Translation;
            this.Rotation = this.Rotation * motion.Rotation;
        }

        public void SetTranslation(UnityEngine.Vector3 _newTranslation)
        {
            this.Translation = _newTranslation;
        }

        public void ApplyWorldSpace(UnityEngine.Transform transform)
        {
            transform.position += this.Translation;
            transform.rotation *= this.Rotation;
        }
    }
}