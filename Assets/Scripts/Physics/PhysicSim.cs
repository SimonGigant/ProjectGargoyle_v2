
namespace PGS.GP.Player
{
    using UnityEngine;

    public class PhysicSim : Game.Component
    {
        public bool isGrounded;
        public bool land;
        public Transform groundCheck;
        public LayerMask groundLayer;
        public float GroundedRadius = .5f;

        private Rigidbody2D rigidbody;
        private IPlayerActor actor;
        private CapsuleCollider2D actorCollider;
        private Collider2D[] resultsCached;
        private MotionComponent motionComponent;

        public override void OnStart()
        {
            rigidbody = GetComponent<Rigidbody2D>();
            resultsCached = new Collider2D[10];
            actorCollider = GetComponent<CapsuleCollider2D>();
            actor = GetComponent<IPlayerActor>();
            motionComponent = GetComponent<MotionComponent>();
        }

        public override void FixedUpdateComponent(float dt)
        {
            CheckGroundCollision();
        }

        private void CheckGroundCollision()
        {
            var wasGrounded = isGrounded;
            isGrounded = false;
            land = false;

            var resultCount = Physics2D.OverlapCircleNonAlloc(groundCheck.position, GroundedRadius, resultsCached, groundLayer);
            for (int i = 0; i < resultCount; i++)
            {
                var collider = resultsCached[i];
                if (collider.gameObject != this.gameObject)
                {
                    isGrounded = true;
                }
            }

            if (wasGrounded != isGrounded)
            {
                if (isGrounded)
                {
                    land = true;
                    actor.Execute(new LandAction());
                }
            }
        }
    }
}
